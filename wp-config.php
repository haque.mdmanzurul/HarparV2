<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'harparV2.com');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'l_:wH)M&+wO;tqu-RvWWQbbo#j$M}:V2s !j&&94]9M+ ]T7eISxRwt%n{N1pW%k');
define('SECURE_AUTH_KEY',  'R)g7DpYpE](E6TW)IpY!f!SiUQ1~cZ)lCoY7k2ixIk[%$-nIKhFKSLQ2=J7?d54L');
define('LOGGED_IN_KEY',    '4q,l0Z0/zESrowwg55`OU#_*i@zNcVp2+r.Ut[J* q>Hx30gKXEQK/ypNn|Kn5y3');
define('NONCE_KEY',        'QMH1 ]Gh4]&4)(<*F[X$@U^]8}U-O+G#NXyGMuOm$!S4l&8<rn5ZcJtbY37V;P<;');
define('AUTH_SALT',        '#C|H=17Q2&aOpwIu>4g]e9srGEH5h+#|u|?UZ*8EXDWi?FCB w<UERFE8+dCg%F-');
define('SECURE_AUTH_SALT', 'o%~Pt,!iH1jQIh}V~PE!Im)S[!}29/Kb|YwL90LZ>/3Pa2@IgZm^ {&?9(CmbY|w');
define('LOGGED_IN_SALT',   '~o|DU/B0A3~kD/U)6=<R]7)9[5KiU|gp<Vfj^M~1^~|4X-km`GfNjJpbJ~/}Y_N#');
define('NONCE_SALT',       'u$YsNS%F*Tz75pT9IwlpBvc-c3msK!%O_pa~{7bt_({4}^-,*nf?VsCuL=kG@-+w');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
