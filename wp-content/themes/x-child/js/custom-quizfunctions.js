/**
 * Created by Manzurul on 1/16/2017.
 */
jQuery(document).ready(function(){
    
    jQuery('#myfield4').change(function(){
        
        if(jQuery.trim(jQuery('#myfield4 option:selected').text())=='I do not have any management experience' ||  jQuery.trim(jQuery('#myfield4 option:selected').text())=='Not applicable'){
            
            var data = {
				'action': 'get_recommended_courses'
			};
            jQuery.post(ajaxurl, data, function(response) {

				if(response.success == 'OK'){
                    console.log(response.showpopup);
                    
                    if(response.showpopup == 'Yes'){
                        jQuery('#place_order').attr('disabled','disabled');
                        
                        
                        jQuery.fancybox({
                          'transitionIn': 'elastic',
                          'transitionOut': 'elastic',
                          'speedIn': 600,
                          'speedOut': 200,
                          'content': response.html,
                          'onClosed' : function(){
                              window.location.reload();
                          }
                        });
                        
                        return false;
                    }
                    if(response.showpopup == 'No' && response.only_course == false){
                        jQuery('#place_order').removeAttr('disabled');


                        jQuery.fancybox({
                            'transitionIn': 'elastic',
                            'transitionOut': 'elastic',
                            'speedIn': 600,
                            'speedOut': 200,
                            'content': response.html,
                            'onClosed' : function(){
                                window.location.reload();
                            }
                        });

                        return false;
                    }
                     
				} 
            });
        } 
        else{
            jQuery('#place_order').removeAttr('disabled');
        }
    }); 
  jQuery( function() {
    jQuery( "#myfield1" ).datepicker({
      changeMonth: true,
      changeYear: true,
      yearRange: "1920:2007"    
    });
  } ); 
    
})

