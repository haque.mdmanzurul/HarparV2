<?php

// =============================================================================
// FUNCTIONS.PHP
// -----------------------------------------------------------------------------
// Overwrite or add your own custom functions to X in this file.
// =============================================================================

// =============================================================================
// TABLE OF CONTENTS
// -----------------------------------------------------------------------------
//   01. Enqueue Parent Stylesheet
//   02. Additional Functions
// =============================================================================

// Enqueue Parent Stylesheet
// =============================================================================

add_filter( 'x_enqueue_parent_stylesheet', '__return_true' );



// Additional Functions
// =============================================================================

add_filter('x_option_x_topbar_content', 'do_shortcode');

add_filter( 'ups_sidebar', 'custom_sidebar_on_pages', 9999 );

function custom_sidebar_on_pages ( $default_sidebar ) {
  if ( in_category('case-studies') ) {
    
    return 'ups-sidebar-case-studies';

  } 

  return $default_sidebar;
}
/**
 * Proper way to enqueue scripts and styles.
 */
function wpdocs_theme_name_scripts() {
    //wp_enqueue_style( 'style-name', get_stylesheet_uri() );
    wp_enqueue_script( 'customquiz', get_template_directory_uri() . '/js/custom-quizfunctions.js', array(), '1.0.0', true );
}
add_action( 'wp_enqueue_scripts', 'wpdocs_theme_name_scripts' );


include_once("inc/general_functions.php");
include_once("inc/api-functions.php");
include_once("inc/quiz.php");

