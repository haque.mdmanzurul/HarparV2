<?php
/**
 * Created by PhpStorm.
 * User: Manzurul
 * Date: 10/20/2016
 * Time: 3:47 PM
 */

//Create course on LMS
if (!function_exists('curl_file_create')) {
    function curl_file_create($filename, $mimetype = '', $postname = '') {
        return "@$filename;filename="
            . ($postname ?: basename($filename))
            . ($mimetype ? ";type=$mimetype" : '');
    }
}
function create_course_in_lms(){
    global $post;
    $post_id = $post->ID;
    $post_type = get_post_type($post_id);


    if($post_type != 'product'){
        return;
    }

    if (isset($post->post_status) && 'auto-draft' == $post->post_status) {
        return;
    }


    $title = $post->post_title;
    $course_ref = esc_attr( $_POST['LMS_course_ref_id'] );
    $qualification = '';
    $description = $post->post_content;
    $course_overview_title = 'Course Overview';
    $course_overview_description = get_field('course_info',$post_id);
    $lms_course_id = get_post_meta($post_id,'lms_course_id', true);

    $category = isset($_POST['tax_input']['product_cat']) ? $_POST['tax_input']['product_cat'] : array(); //tax_input[product_cat][]
    $qualifications = isset($_POST['tax_input']['qualifications']) ? $_POST['tax_input']['qualifications'] : array();
    $producttype = isset($_POST['tax_input']['taxproducttype']) ? $_POST['tax_input']['taxproducttype'] : array();
    
    $files= isset($_POST['_wc_file_urls']) ? $_POST['_wc_file_urls'] : array();
    $file_names= isset($_POST['_wc_file_names']) ? $_POST['_wc_file_names'] : array();

   

    if(in_array(59, $producttype)) // courses
        $type = 1;
    else if(in_array(61, $producttype)) // qualifications
        $type = 2;
    else if(in_array(60, $producttype))
        $type = 3;

    /*if(count($category) > 0 and in_array('13',$category))
        return;*/


    if(count($qualifications) > 0){
        $term = get_term( $qualifications[1], 'qualifications' );
        $qualification = $term->name;
    }

   $cfile = ('@' . realpath($files[0]));

// Assign POST data
    $data = array(
        'apikey'=>'389c16b45bddff79be4b31b04c1f52eb4e91bb83',
        'title'=>$title,
        'description'=>$description,
        'refno'=>$course_ref,
        'qualification'=>$qualification,
        'course_overview'=>$course_overview_title,
        'course_overview_details'=>$course_overview_description,
        'ecommerce_course_id'=>$post_id,
        'type'=>$type,
        'lms_course_id'=>$lms_course_id,
        'file0'=>$files[0],
        'filename0'=>$file_names[0]
    );
    
    /*if(count($files)>0){
        $data['filecount'] = count($files);
        
        for($i = 0; $i < count($files); $i++){
            $data['file'+$i] = '@' . realpath($files[$i]);
            $data['filename'+$i] = $file_names[$i];
        }
    }*/



    $ch = curl_init("http://lms.harpar.com/api/v1/createCourse");
    curl_setopt($ch, CURLOPT_HEADER,0);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
        'Authorization: 389c16b45bddff79be4b31b04c1f52eb4e91bb83',
        'Accept: application/json',
        'Content-Type: multipart/form-data;'
    ));
    curl_setopt($ch, CURLOPT_SAFE_UPLOAD, false);

    curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    $result = curl_exec($ch);
    curl_close($ch);
    $data = json_decode($result);
  
    update_post_meta( $post_id, 'lms_course_id', $data->courseId);


}
add_action( 'save_post', 'create_course_in_lms', 10 );


function lms_course_data_get_meta( $value ) {
    global $post;

    $field = get_post_meta( $post->ID, $value, true );
    if ( ! empty( $field ) ) {
        return is_array( $field ) ? stripslashes_deep( $field ) : stripslashes( wp_kses_decode_entities( $field ) );
    } else {
        return false;
    }
}

function lms_course_data_add_meta_box() {
    add_meta_box(
        'lms_course_data-lms-course-data',
        __( 'LMS Course Data', 'lms_course_data' ),
        'lms_course_data_html',
        'product',
        'side',
        'default'
    );
}
add_action( 'add_meta_boxes', 'lms_course_data_add_meta_box' );

function lms_course_data_html( $post) {
    wp_nonce_field( '_lms_course_data_nonce', 'lms_course_data_nonce' ); ?>

    <p>
        <label for="lms_course_data_lms_course_ref_no"><?php _e( 'LMS Course Ref No', 'lms_course_data' ); ?></label><br>
        <input type="text" name="LMS_course_ref_id" id="LMS_course_ref_id" value="<?php echo lms_course_data_get_meta( 'LMS_course_ref_id' ); ?>">
    </p>

    <?php
}

function lms_course_data_save( $post_id ) {

    if ( ! isset( $_POST['lms_course_data_nonce'] ) || ! wp_verify_nonce( $_POST['lms_course_data_nonce'], '_lms_course_data_nonce' ) ) return;
    if ( ! current_user_can( 'edit_post', $post_id ) ) return;




    if ( isset( $_POST['LMS_course_ref_id'] ) )
        update_post_meta( $post_id, 'LMS_course_ref_id', esc_attr( $_POST['LMS_course_ref_id'] ) );




}
add_action( 'save_post', 'lms_course_data_save' );


//Synchronise the order
add_action( 'woocommerce_thankyou', 'set_user_courses_on_lms');

function set_user_courses_on_lms( $order_id ){

    if ( ! $order_id ) {
        return;
    }

    $order = new WC_Order( $order_id );
    $order->update_status( 'completed' );


    $billing_first_name =  get_post_meta($order_id,'_billing_first_name',true);
    $billing_last_name = get_post_meta($order_id,'_billing_last_name',true);
    $billing_address = get_post_meta($order_id,'_billing_address_1',true);
    $billing_address2 = get_post_meta($order_id,'_billing_address_2',true);
    $billing_city = get_post_meta($order_id,'_billing_city',true);
    $billing_postcode = get_post_meta($order_id,'_billing_postcode',true);
    //$billing_country = get_post_meta($order_id,'_billing_country',true);
    $billing_state = get_post_meta($order_id,'_billing_state',true);
    $billing_email = get_post_meta($order_id,'_billing_email',true);
    //$billing_phone = get_post_meta($order_id,'_billing_phone',true);

    //assign user courses on LMS
    $items = $order->get_items();
    $course_ids = array();
    foreach ( $items as $item ) {
        $product_id = $item['product_id'];
        $lms_course_id = get_post_meta($product_id,'lms_course_id',true);
        $categories = get_the_category( $product_id );
       
        array_push($course_ids,$lms_course_id);  // do something


    }


        if(count($course_ids) == 0)
            return;
        //check if user exist
        $data = array(
            'apikey'=>'389c16b45bddff79be4b31b04c1f52eb4e91bb83',
            'email'=>$billing_email
        );

        $ch = curl_init("http://lms.harpar.com/api/v1/checkuser");
        curl_setopt($ch, CURLOPT_HEADER,0);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Authorization: 389c16b45bddff79be4b31b04c1f52eb4e91bb83',
            'Accept: application/json'
        ));

        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $result = curl_exec($ch);
        curl_close($ch);
        $decodedata = json_decode($result);

        $lms_user_id = '';

            if($decodedata->success == "FAIL"){
                //check if user exist
                $data = array(
                    'apikey'=>'389c16b45bddff79be4b31b04c1f52eb4e91bb83',
                    'firstname'=>$billing_first_name,
                    'lastname'=>$billing_last_name,
                    'email'=>$billing_email,
                    'username'=>$billing_email,
                    'address1'=>$billing_address,
                    'address2'=>$billing_address2,
                    'town'=>$billing_city,
                    'district'=>$billing_state,
                    'postcode'=>$billing_postcode,
                    //'phone'=>$billing_phone

                );

                $ch = curl_init("http://lms.harpar.com/api/v1/createUser");
                curl_setopt($ch, CURLOPT_HEADER,0);
                curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                    'Authorization: 389c16b45bddff79be4b31b04c1f52eb4e91bb83',
                    'Accept: application/json'
                ));

                curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                $result = curl_exec($ch);
                curl_close($ch);
                $userdata = json_decode($result);
                $lms_user_id = $userdata->userID;
            }else{

                $userdata = json_decode($result);
                $lms_user_id = $userdata->user_id;
            }

            //echo $lms_user_id;
            //end check user



            $lms_courses_id = implode(',',$course_ids);
            $data = array(
                'apikey'=>'389c16b45bddff79be4b31b04c1f52eb4e91bb83',
                'user_id'=>$lms_user_id,
                'user_courses'=>$lms_courses_id
            );

            $ch = curl_init("http://lms.harpar.com/api/v1/assignCourses");
            curl_setopt($ch, CURLOPT_HEADER,0);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Authorization: 389c16b45bddff79be4b31b04c1f52eb4e91bb83',
                'Accept: application/json'
            ));

            curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            $result = curl_exec($ch);
            curl_close($ch);
            

}

/*
	Usage: lms_course_data_get_meta( 'lms_course_data_lms_course_ref_no' )
	Usage: lms_course_data_get_meta( 'lms_course_data_lms_qualification' )
*/


