<?php
/**
* Generated by the WordPress Meta Box generator
* at http://jeremyhixon.com/tool/wordpress-meta-box-generator/
*/

function select_quiz_get_meta( $value ) {
global $post;

$field = get_post_meta( $post->ID, $value, true );
if ( ! empty( $field ) ) {
return is_array( $field ) ? stripslashes_deep( $field ) : stripslashes( wp_kses_decode_entities( $field ) );
} else {
return false;
}
}

function select_quiz_add_meta_box() {
add_meta_box(
'select_quiz-select-quiz',
__( 'Course Questionaire Settings', 'select_quiz' ),
'select_quiz_html',
'product',
'side',
'default'
);
}
add_action( 'add_meta_boxes', 'select_quiz_add_meta_box' );

function select_quiz_html( $post) {
    wp_nonce_field( '_select_quiz_nonce', 'select_quiz_nonce' );
    global $wpdb;
    $quizs = $wpdb->get_results('SELECT * FROM '.$wpdb->prefix.'mtouchquiz_quiz')
?> 
<p>
        <label for="select_quiz_select_a_quiz"><?php _e( 'Select FOR LMS', 'select_quiz' ); ?></label><br>
        <select name="select_quiz_select_lms_quiz" id="select_quiz_select_lms_quiz">
            <option value="">Select a Questionaire </option>
            <?php

            if(count($quizs)>0){
                foreach($quizs as $qz){
                    ?>
                    <option value="<?php echo $qz->ID ?>" <?php echo (select_quiz_get_meta( 'select_quiz_select_lms_quiz' ) === $qz->ID ) ? 'selected' : '' ?>><?php echo $qz->name ?></option>
                    <?php
                }
            }

            ?>

        </select>
    </p>
    <?php
}

function select_quiz_save( $post_id ) {
    if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) return;
    if ( ! isset( $_POST['select_quiz_nonce'] ) || ! wp_verify_nonce( $_POST['select_quiz_nonce'], '_select_quiz_nonce' ) ) return;
    if ( ! current_user_can( 'edit_post', $post_id ) ) return;


    if ( isset( $_POST['select_quiz_select_lms_quiz'] ) ){
        update_post_meta( $post_id, 'select_quiz_select_lms_quiz', esc_attr( $_POST['select_quiz_select_lms_quiz'] ) );
    }

}
add_action( 'save_post', 'select_quiz_save' );

/*
	Usage: select_quiz_get_meta( 'select_quiz_select_a_quiz' )
*/

/**
 * Proper way to enqueue scripts and styles.
 */
function questionaire_theme_name_scripts() {
    wp_enqueue_script( 'customquizfucntion', get_stylesheet_directory_uri() . '/js/custom-quizfunctions.js', array(), '1.0.0', true );
}
add_action( 'wp_enqueue_scripts', 'questionaire_theme_name_scripts' );


add_action( 'wp_ajax_get_product_type_for_cart_add', 'get_product_type_for_cart_add' );
add_action( 'wp_ajax_nopriv_get_product_type_for_cart_add', 'get_product_type_for_cart_add' );

function get_product_type_for_cart_add(){
    $product_id = $_REQUEST['product_id'];
    $quiz_id = get_post_meta( $product_id, 'select_quiz_select_a_quiz', true );
    $types = get_the_terms( $product_id, 'taxproducttype' );
    $questionaire_permalink = get_permalink(1338);
    //$questionaire_permalink = get_permalink(1343);

    $course_has_questionaire = false;
    if(count($types)>0){
        foreach($types as $tp){
            if($tp->name == "Accreditations" OR $tp->name == 'Qualifications'){
                $course_has_questionaire = true;
                if($quiz_id == '')
                    $quiz_id = 1;
                break;
            }
        }
    }

    if($course_has_questionaire)
        wp_send_json(array('success'=>'OK','link'=>$questionaire_permalink.'?product_id='.$product_id.'&questionaire='.$quiz_id));
    else
        wp_send_json(array('success'=>'NOK','link'=>$questionaire_permalink));
}


add_action( 'wp_ajax_get_recommended_courses', 'get_recommended_courses' );
add_action( 'wp_ajax_nopriv_get_recommended_courses', 'get_recommended_courses' );

add_action('before_woocommerce_pay','get_recommended_courses',10,0);
function get_recommended_courses(){
    /*
    $args = array(
    'post_type'             => 'product',
    'post_status'           => 'publish',
    'posts_per_page'    =>1000,
    'taxproducttype' => 'courses',
);
$loop = new WP_Query($args);
    $i = 1;
$ee = array();
while ( $loop->have_posts() ) : $loop->the_post();
    $i++;
    array_push($ee, get_the_ID());
endwhile;    
wp_send_json(array('success'=>'OK','showpopup'=>$showpopup,'count'=>$i,'html'=>implode('||',$ee)));
*/
    $product_ids= array();
    $cart_items  = WC()->cart->cart_contents;
    $html = '';
    $only_course = true;
    //print_r();
    if( count($cart_items)>0 ) {
      $showpopup = 'Yes';    
      foreach ( WC()->cart->get_cart() as $cart_item_key => $cart_item){
        $product_id = $cart_item['product_id'];  
        $prod_id = ( isset( $prod_in_cart['variation_id'] ) && $prod_in_cart['variation_id'] != 0 ) ? $prod_in_cart['variation_id'] : $prod_in_cart['product_id'];
        $types = get_the_terms( $product_id, 'taxproducttype' );
  
        if(count($types)>0){
            foreach($types as $tp){
                if($tp->name == "Accreditations" OR $tp->name == 'Qualifications'){
                    $only_course = false;
                    //$html .= $product_id.',';
                    $downgradeable_courses   =   get_field('related_downgradeable_courses',$product_id );
                    //print_r($crosssellProductIds);
                    //$crosssellProductIds    =   $crosssellProductIds[0];
                    if(is_array($downgradeable_courses)){
                        

                        foreach($downgradeable_courses as $prod){
                            $product_info =  wc_get_product( $prod );
                            $html .='<div class="product">';
                            $html .='<div class="product-inner">';
                            $html .='<div class="product-title esg-content eg-post-1252 eg-grant-element-0">'.$product_info->get_title().'</div>';
                            $src = wp_get_attachment_image_src( get_post_thumbnail_id($prod),'full', false);

                            $html .='<div class="product-images"><img width="100%" src="'.$src[0].'" class=""></div>';
                            $html .='<div class="product-price esg-content eg-post-1252 eg-grant-element-3">'.get_woocommerce_currency_symbol().$product_info->get_regular_price().'</div>';
                            $html.='<div class="product-view"><a class="checkout-button button alt wc-forward eg-grant-element-23 eg-post-'.$prod.'" href="'.get_permalink($prod).'" target="_blank">View course</a></div>';

                            $html .='</div>';
                            $html .='</div>';
                        }
                       
 
                    }
                    WC()->cart->set_quantity( $cart_item_key, $cart_item['quantity'] - 1, true  );
                    //$prod_unique_id = WC()->cart->generate_cart_id($prod_id);
                    // Remove it from the cart by un-setting it
                    //unset( WC()->cart->cart_contents[$prod_unique_id] );
                    //WC()->cart->remove_cart_item($product_id);
                }
            }
        }  
        
      }  
    }





    if($html==''){
        $showpopup = 'No';
        $html = '<div class="recommended_products downgradeable"><h2>You are attempting to purchase one of our senior level courses. These require management experience to be able to complete the assessments. You may view more <a href="'.get_bloginfo('url').'/accreditations">Accreditations</a> and <a href="'.get_bloginfo('url').'/awarding-body">Qualifications</a></h2></div>';
    }else{
        $html = '<div class="recommended_products downgradeable"><h2>You are attempting to purchase one of our senior level courses. These require management experience to be able to complete the assessments. You may find the following courses more suitable.</h2><br/>'.$html.'</div>';   
    }

    wp_send_json(array('success'=>'OK','showpopup'=>$showpopup,'html'=>$html,'only_course'=>$only_course));
}

function show_questionaire(){

    $product_id = $_REQUEST['product_id'];
    $questionaire_id = $_REQUEST['questionaire'];

    return do_shortcode("[mtouchquiz id=$questionaire_id product_id=$product_id]");

}

add_shortcode('Questionaire_Quiz','show_questionaire');

function so_validate_add_cart_item() {

    // do your validation, if not met switch $passed to false
    $product_id = $_REQUEST['add-to-cart'];
    $quiz_id = get_post_meta( $product_id, 'select_quiz_select_a_quiz', true );
    $types = get_the_terms( $product_id, 'taxproducttype' );
    //$questionaire_permalink = get_permalink(1338);
    $questionaire_permalink = get_permalink(1343);

    $course_has_questionaire = false;
    if(count($types)>0){
        foreach($types as $tp){
            if($tp->name == "Accreditations" OR $tp->name == 'Qualifications'){
                wp_redirect($questionaire_permalink.'?product_id='.$product_id.'&questionaire='.$quiz_id);
                break;
            }
        }
    }

}
//add_filter( 'woocommerce_add_cart_item', 'so_validate_add_cart_item', 10, 5 );

add_action( 'wp_ajax_getQuestionareLMS', 'getQuestionareLMS' );
add_action( 'wp_ajax_nopriv_getQuestionareLMS', 'getQuestionareLMS' );

function getQuestionareLMS(){
    global $wpdb;

    $post_id = $_REQUEST['post_id'];

    $quiz_id =  get_post_meta($post_id,'select_quiz_select_lms_quiz',true);
    //echo $quiz_id;
    //exit;    
    //wp_send_json(array('quiz_id'=>$options->meta_value));
    if($quiz_id == '')
        $quiz_id = 2;

    $questions = $wpdb->get_results('SELECT * FROM '.$wpdb->prefix.'mtouchquiz_question WHERE quiz_id='.$quiz_id);

    $return_data = array();

    if(count($questions)>0){
        foreach($questions as $q){
            $data = array();

            $data['question'] = $q->question;
            $data['options'] = array('True','False');
            $answers = $wpdb->get_row('SELECT * FROM '.$wpdb->prefix.'mtouchquiz_answer WHERE  question_id='.$q->ID);
            if(count($answers)>0){
                if($answers->correct == 1)
                    $data['answer'] = 0;
                else
                    $data['answer'] = 1;
            }

            array_push($return_data, $data);
        }
    }

    wp_send_json($return_data);
}

