/**
 * Created by Manzurul on 12/29/2016.
 */
var baseurl = ajaxurl+'?action=';


var contractsapp = angular.module('contractRecords', ['ui.bootstrap'])
    .constant('API_URL', baseurl);

var customersapp = angular.module('customerRecords', ['ui.bootstrap','ui.utils'])
    .constant('API_URL', baseurl);

var paymentschedulesapp = angular.module('paymentscheduleRecords', ['ui.bootstrap','ui.utils'])
    .constant('API_URL', baseurl);

var servicesapp = angular.module('serviceRecords', ['ui.bootstrap','ui.utils'])
    .constant('API_URL', baseurl);

var categoriesapp = angular.module('categoryRecords', ['ui.bootstrap','ui.utils'])
    .constant('API_URL', baseurl);

var staffsapp = angular.module('staffRecords', ['ui.bootstrap','ui.utils'])
    .constant('API_URL', baseurl);

var contractservicesapp = angular.module('contractserviceRecords', ['ui.bootstrap','ui.utils'])
    .constant('API_URL', baseurl);

var paymentsapp = angular.module('paymentRecords', ['ui.bootstrap','ui.utils'])
    .constant('API_URL', baseurl);

jQuery(document).ready(function(){

});
