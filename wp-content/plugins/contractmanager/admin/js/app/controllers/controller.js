/**
 * Created by Manzurul on 12/29/2016.
 */

//contracts app
contractsapp.controller('contractsController', function($scope, $http, API_URL) {
    console.log('Initail Call is working 1');
    //retrieve employees listing from API'

    $http({
        url:API_URL + "contracts",
        type:'POST'})
        .then(function(response) {
            console.log(response);
            //$scope.contracts = jQuery.makeArray( response.data.contracts );
            console.log($scope.contracts);
            $scope.customer = {
                model: null,
                all: response.data.customers
            };

            if (!$scope.$$phase) {
                //$scope.$apply();
            }
            jQuery('#contractsTable').DataTable({
                data: response.data.contracts,
                columns: [
                    { data: 'contract_id' },
                    { data: 'customer_name' },
                    { data: 'contract_title' },
                    { data: 'contract_price' },
                    { data: 'start_dt' },
                    { data: 'end_dt' },
                    { data: 'signed_dt' },
                    { data: 'signed_by' },
                    { data: null }
                ],
                "columnDefs": [ {
                    "targets": 8,
                    "data": null,
                    "defaultContent": '<a href="admin.php?page=all-contracts&subpage=manageservices&contractid='+contract_id+'" class="button button-small">Manage Services</a><a href="#" class="button button-small" ng-click="toggle(edit, contract)">Edit</a><a href="#" class="button button-small btn-danger" contract_index="{{ index }}" ng-click="confirmDelete(contract)">Delete</a>'
                } ]
            } );

        });



    //show modal form
    $scope.toggle = function(modalstate, contract) {
        console.log(contract.contract_id);

        $scope.modalstate = modalstate;
        jQuery('#myFormModal').addClass('in').removeClass('modal');
        console.log('working on modal state');
        switch (modalstate) {
            case 'add':
                $scope.form_title = "Add New Contract";

                break;
            case 'edit':
                $scope.form_title = "Contract Detail";
                $http.get(API_URL + 'contractModify&id=' + contract.contract_id)
                    .then(function(response) {
                        console.log(response.data);
                        $scope.contract = response.data;
                    });
                break;
            default:
                break;
        }


    }

    //save new record / update existing record
    $scope.save = function(contract) {
        var url = API_URL + "contractSave";

        $http({
            method: 'POST',
            url: url,
            data: jQuery.param($scope.contract),
            headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        }).then(function(response) {
            if(response.data.mode == 'edit'){
                angular.forEach($scope.contracts, function(u, i) {
                    console.log(u)
                    if (u.contract_id == response.data.contract_id ) {
                        console.log('scope data change')
                        $scope.contracts[i] = response.data;
                        if (!$scope.$$phase) {
                            $scope.$apply();
                        }
                        jQuery('#myFormModal').addClass('modal').removeClass('in');
                    }
                });
            }else{
                jQuery('#myFormModal').addClass('modal').removeClass('in');
                $scope.contracts.push(response.data);
            }

        });
    }

    //delete record
    $scope.confirmDelete = function(contract) {
        console.log('Delete start'+contract.contract_id);
        var contractid = '';
        var isConfirmDelete = confirm('Are you sure you want this contract?');
        if (isConfirmDelete) {
            $http({
                method: 'DELETE',
                url: API_URL + 'contractDelete&id=' + contract.contract_id
            }).
            then(function(response) {
                if(response.data.success == 'OK'){
                    var index = $scope.contracts.indexOf(contract);
                    if (index != -1) {
                        $scope.contracts.splice(index, 1);
                    }
                }
                else{
                    alert('Contract can not be deleted.');
                }

                //$scope.contracts.splice($scope.contracts.indexOf($scope.contract), 1);
                console.log('delete done');
                //location.reload();
            }).
            error(function(data) {
                console.log(data);
                alert('Unable to delete');
            });
        } else {
            return false;
        }
    }


    $scope.close = function(Class){
        jQuery('#myFormModal').addClass('modal').removeClass('in');
    }
});


//customers app
customersapp.controller('customersController', function($scope, $http, API_URL) {
    console.log('Initail Call is working 1');
    //retrieve employees listing from API'
    $scope.activeData = {
        model: null,
        data: [
            {value: '1', label: 'Yes'},
            {value: '0', label: 'No'}
        ]
    };
    $http({
        url:API_URL + "customers",
        type:'POST'})
        .then(function(response) {
            console.log(response);
            $scope.customers = response.data.customers;

            if (!$scope.$$phase) {
                $scope.$apply();
            }
        });


    //show modal form
    $scope.toggle = function(modalstate, customer) {
        console.log(customer.customer_id);

        $scope.modalstate = modalstate;
        jQuery('#myFormModal').addClass('in').removeClass('modal');
        console.log('working on modal state');
        switch (modalstate) {
            case 'add':
                $scope.form_title = "Add New Customer";

                break;
            case 'edit':
                $scope.form_title = "Customer Detail";
                $http.get(API_URL + 'customerModify&id=' + customer.customer_id)
                    .then(function(response) {
                        console.log(response.data);
                        $scope.customer = response.data;
                    });
                break;
            default:
                break;
        }


    }

    //save new record / update existing record
    $scope.save = function(customer) {
        var url = API_URL + "customerSave";

        $http({
            method: 'POST',
            url: url,
            data: jQuery.param($scope.customer),
            headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        }).then(function(response) {
            if(response.data.mode == 'edit'){
                angular.forEach($scope.customers, function(u, i) {
                    console.log(u)
                    if (u.customer_id == response.data.customer_id ) {
                        console.log('scope data change')
                        $scope.customers[i] = response.data;
                        if (!$scope.$$phase) {
                            $scope.$apply();
                        }
                        jQuery('#myFormModal').addClass('modal').removeClass('in');
                    }
                });
            }else{
                jQuery('#myFormModal').addClass('modal').removeClass('in');
                $scope.customers.push(response.data);
            }

        });
    }

    //delete record
    $scope.confirmDelete = function(customer) {
        console.log('Delete start'+customer.customer_id);
        var customerid = '';
        var isConfirmDelete = confirm('Are you sure you want this record?');
        if (isConfirmDelete) {
            $http({
                method: 'DELETE',
                url: API_URL + 'customerDelete&id=' + customer.customer_id
            }).
            then(function(response) {
                var array = response;
                if(array.data.success == 'NO'){
                    alert('Unsuccessful!!! This client has one or more contract.');
                    return;
                }
                if(array.data.success == 'OK'){
                    $scope.customers.splice($scope.customers.indexOf(customer), 1);
                    console.log('delete done')
                }

                //location.reload();
            }).
            error(function(data) {
                console.log(data);
                alert('Unable to delete');
            });
        } else {
            return false;
        }
    }

    $scope.close = function(Class){
        jQuery('#myFormModal').addClass('modal').removeClass('in');
    }
});

//customers app
paymentschedulesapp.controller('paymentschedulesController', function($scope, $http, API_URL) {
    console.log('Initail Call is working 1');
    //retrieve employees listing from API'

    $http({
        url:API_URL + "paymentschedules",
        type:'POST'})
        .then(function(response) {
            console.log(response);
            $scope.paymentschedules = response.data.paymentschedules;
            $scope.contracts = {
                model: null,
                all: response.data.contracts
            };
            if (!$scope.$$phase) {
                $scope.$apply();
            }
        });


    //show modal form
    $scope.toggle = function(modalstate, paymentschedule) {
        console.log(paymentschedule.pymt_sch_id);

        $scope.modalstate = modalstate;
        jQuery('#myFormModal').addClass('in').removeClass('modal');
        console.log('working on modal state');
        switch (modalstate) {
            case 'add':
                $scope.form_title = "Add New paymentschedule";

                break;
            case 'edit':
                $scope.form_title = "Employee Detail";
                $http.get(API_URL + 'paymentscheduleModify&id=' + paymentschedule.pymt_sch_id)
                    .then(function(response) {
                        console.log(response.data);
                        $scope.paymentschedule = response.data;
                    });
                break;
            default:
                break;
        }


    }

    //save new record / update existing record
    $scope.save = function(paymentschedule) {
        var url = API_URL + "paymentscheduleSave";

        $http({
            method: 'POST',
            url: url,
            data: jQuery.param($scope.paymentschedule),
            headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        }).then(function(response) {
            if(response.data.mode == 'edit'){
                angular.forEach($scope.paymentschedules, function(u, i) {
                    console.log(u)
                    if (u.pymt_sch_id == response.data.pymt_sch_id ) {
                        console.log('scope data change')
                        $scope.paymentschedules[i] = response.data;
                        if (!$scope.$$phase) {
                            $scope.$apply();
                        }
                        jQuery('#myFormModal').addClass('modal').removeClass('in');
                    }
                });
            }else{
                jQuery('#myFormModal').addClass('modal').removeClass('in');
                $scope.paymentschedules.push(response.data);
            }

        });
    }

    //delete record
    $scope.confirmDelete = function(paymentschedule) {
        console.log('Delete start'+paymentschedule.pymt_sch_id);
        var paymentscheduleid = '';
        var isConfirmDelete = confirm('Are you sure you want this record?');
        if (isConfirmDelete) {
            $http({
                method: 'DELETE',
                url: API_URL + 'paymentscheduleDelete&id=' + paymentschedule.pymt_sch_id
            }).
            then(function(response) {
                var array = response;
                if(array.data.success == 'NO'){
                    alert('One of Contract still associated with this client');
                    return;
                }
                console.log(array.data.success);
                if(array.data.success == 'OK'){
                    $scope.paymentschedules.splice($scope.paymentschedules.indexOf(paymentschedule), 1);
                    console.log('delete done')
                }

                //location.reload();
            }).
            error(function(data) {
                console.log(data);
                alert('Unable to delete');
            });
        } else {
            return false;
        }
    }

    $scope.close = function(Class){
        jQuery('#myFormModal').addClass('modal').removeClass('in');
    }
});


//services app
servicesapp.controller('servicesController', function($scope, $http, API_URL) {
    console.log('Initail Call is working 1');
    //retrieve employees listing from API'
    $http({
        url:API_URL + "services",
        type:'POST'})
        .then(function(response) {
            console.log(response);
            $scope.services = response.data.services;
            $scope.servicecategory = {
                model: null,
                all: response.data.service_categories
            };
            if (!$scope.$$phase) {
                $scope.$apply();
            }
        });


    //show modal form
    $scope.toggle = function(modalstate, service) {

        $scope.modalstate = modalstate;
        jQuery('#myFormModal').addClass('in').removeClass('modal');
        console.log('working on modal state');
        switch (modalstate) {
            case 'add':
                $scope.form_title = "Add New Service";
                console.log($scope.servicecategory.all)
                break;
            case 'edit':
                $scope.form_title = "Service Detail";
                $http.get(API_URL + 'serviceModify&id=' + service.service_id)
                    .then(function(response) {
                        console.log(response.data);
                        $scope.service = response.data;
                    });
                break;
            default:
                break;
        }


    }

    //save new record / update existing record
    $scope.save = function(service) {
        var url = API_URL + "serviceSave";

        $http({
            method: 'POST',
            url: url,
            data: jQuery.param($scope.service),
            headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        }).then(function(response) {
            if(response.data.mode == 'edit'){
                angular.forEach($scope.services, function(u, i) {
                    console.log(u)
                    if (u.service_id == response.data.service_id ) {
                        console.log('scope data change')
                        $scope.services[i] = response.data;
                        if (!$scope.$$phase) {
                            $scope.$apply();
                        }
                        jQuery('#myFormModal').addClass('modal').removeClass('in');
                    }
                });
            }else{
                jQuery('#myFormModal').addClass('modal').removeClass('in');
                $scope.services.push(response.data);
            }

        });
    }

    //delete record
    $scope.confirmDelete = function(service) {
        console.log('Delete start'+service.service_id);
        var serviceid = '';
        var isConfirmDelete = confirm('Are you sure you want this record?');
        if (isConfirmDelete) {
            $http({
                method: 'DELETE',
                url: API_URL + 'serviceDelete&id=' + service.service_id
            }).
            then(function(response) {
                if(response.data.success == 'OK'){
                    var index = $scope.services.indexOf(service);
                    if (index != -1) {
                        $scope.services.splice(index, 1);
                    }
                }
                else{
                    alert('Service can not be deleted due to associated with category.');
                }

                //$scope.services.splice($scope.services.indexOf($scope.service), 1);
                console.log('delete done');
                //location.reload();
            }).
            error(function(data) {
                console.log(data);
                alert('Unable to delete');
            });
        } else {
            return false;
        }
    }

    $scope.close = function(Class){
        jQuery('#myFormModal').addClass('modal').removeClass('in');
    }
});

//Category app
categoriesapp.controller('categoriesController', function($scope, $http, API_URL) {
    console.log('Initail Call is working 1');
    //retrieve employees listing from API'
    $scope.activeData = {
        model: null,
        data: [
            {value: '1', label: 'Yes'},
            {value: '0', label: 'No'}
        ]
    };
    $http({
        url:API_URL + "categories",
        type:'POST'})
        .then(function(response) {
            console.log(response);
            $scope.categories = response.data.categories;

            if (!$scope.$$phase) {
                $scope.$apply();
            }
        });


    //show modal form
    $scope.toggle = function(modalstate, category) {
        $scope.modalstate = modalstate;
        jQuery('#myFormModal').addClass('in').removeClass('modal');
        console.log('working on modal state');
        switch (modalstate) {
            case 'add':
                $scope.form_title = "Add New Category";

                break;
            case 'edit':
                $scope.form_title = "Category Detail";
                $http.get(API_URL + 'categoryModify&id=' + category.serv_cat_id)
                    .then(function(response) {
                        $scope.category = response.data;
                    });
                break;
            default:
                break;
        }


    }

    //save new record / update existing record
    $scope.save = function(category) {
        var url = API_URL + "categorySave";

        $http({
            method: 'POST',
            url: url,
            data: jQuery.param($scope.category),
            headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        }).then(function(response) {
            if(response.data.mode == 'edit'){
                angular.forEach($scope.categories, function(u, i) {
                    if (u.serv_cat_id == response.data.serv_cat_id ) {
                        console.log('scope data change')
                        $scope.categories[i] = response.data;
                        if (!$scope.$$phase) {
                            $scope.$apply();
                        }
                        jQuery('#myFormModal').addClass('modal').removeClass('in');
                    }
                });
            }else{
                jQuery('#myFormModal').addClass('modal').removeClass('in');
                $scope.categories.push(response.data);
            }

        });
    }

    //delete record
    $scope.confirmDelete = function(category) {
        var categoryid = '';
        var isConfirmDelete = confirm('Are you sure you want this record?');
        if (isConfirmDelete) {
            $http({
                method: 'DELETE',
                url: API_URL + 'customerDelete&id=' + category.serv_cat_id
            }).
            then(function(response) {
                var array = response;
                if(array.data.success == 'NO'){
                    alert('Category cannot be deleted');
                    return;
                }
                if(array.data.success == 'OK'){
                    $scope.categories.splice($scope.categories.indexOf(category), 1);
                    console.log('delete done')
                }

                //location.reload();
            }).
            error(function(data) {
                console.log(data);
                alert('Unable to delete');
            });
        } else {
            return false;
        }
    }

    $scope.close = function(Class){
        jQuery('#myFormModal').addClass('modal').removeClass('in');
    }
});


//Staff app
staffsapp.controller('staffsController', function($scope, $http, API_URL) {
    console.log('Initail Call is working 1');
    //retrieve employees listing from API'
    $scope.activeData = {
        model: null,
        data: [
            {value: '1', label: 'Yes'},
            {value: '0', label: 'No'}
        ]
    };
    $http({
        url:API_URL + "staffs",
        type:'POST'})
        .then(function(response) {
            console.log(response);
            $scope.staffs = response.data.staffs;

            if (!$scope.$$phase) {
                $scope.$apply();
            }
        });


    //show modal form
    $scope.toggle = function(modalstate, staff) {
        $scope.modalstate = modalstate;
        jQuery('#myFormModal').addClass('in').removeClass('modal');
        console.log('working on modal state');
        switch (modalstate) {
            case 'add':
                $scope.form_title = "Add New Employee";

                break;
            case 'edit':
                $scope.form_title = "Employee Detail";
                $http.get(API_URL + 'staffModify&id=' + staff.staff_id)
                    .then(function(response) {
                        $scope.staff = response.data;
                    });
                break;
            default:
                break;
        }


    }

    //save new record / update existing record
    $scope.save = function(staff) {
        var url = API_URL + "staffSave";

        $http({
            method: 'POST',
            url: url,
            data: jQuery.param($scope.staff),
            headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        }).then(function(response) {
            if(response.data.mode == 'edit'){
                angular.forEach($scope.staffs, function(u, i) {
                    if (u.staff_id == response.data.staff_id ) {
                        console.log('scope data change')
                        $scope.staffs[i] = response.data;
                        if (!$scope.$$phase) {
                            $scope.$apply();
                        }
                        jQuery('#myFormModal').addClass('modal').removeClass('in');
                    }
                });
            }else{
                jQuery('#myFormModal').addClass('modal').removeClass('in');
                $scope.staffs.push(response.data);
            }

        });
    }

    //delete record
    $scope.confirmDelete = function(staff) {
        var staffid = '';
        var isConfirmDelete = confirm('Are you sure you want this record?');
        if (isConfirmDelete) {
            $http({
                method: 'DELETE',
                url: API_URL + 'staffDelete&id=' + staff.staff_id
            }).
            then(function(response) {
                var array = response;
                if(array.data.success == 'NO'){
                    alert('One of Contract still associated with this Employee');
                    return;
                }
                if(array.data.success == 'OK'){
                    $scope.staffs.splice($scope.staffs.indexOf(staff), 1);
                    console.log('delete done')
                }

                //location.reload();
            }).
            error(function(data) {
                console.log(data);
                alert('Unable to delete');
            });
        } else {
            return false;
        }
    }
});

//contract contractservices app
contractservicesapp.controller('contractservicesController', function($scope, $http, API_URL) {
    console.log('Initail Call is working 1');
    $tokenurl = window.location.href;
    $tokenurl = $tokenurl.split('=');
    //retrieve employees listing from API'
    $scope.activeData = {
        model: null,
        data: [
            {value: '1', label: 'Yes'},
            {value: '0', label: 'No'}
        ]
    };
    $http({
        url:API_URL + "contractservices&contractid="+$tokenurl[$tokenurl.length-1],
        type:'POST'})
        .then(function(response) {
            console.log(response);
            $scope.contractservices = response.data.contractservices;
            $scope.contract = {
                model: null,
                all: response.data.contracts
            };
            $scope.service = {
                model: null,
                all: response.data.services
            };
            $scope.staff = {
                model: null,
                all: response.data.staffs
            };
            if (!$scope.$$phase) {
                $scope.$apply();
            }
        });


    //show modal form
    $scope.toggle = function(modalstate, contractservice) {

        $scope.modalstate = modalstate;
        jQuery('#myFormModal').addClass('in').removeClass('modal');
        console.log('working on modal state');
        switch (modalstate) {
            case 'add':
                $scope.form_title = "Add New Contract Service";
                $scope.contractservice = null;
                //console.log(contractservice);
                break;
            case 'edit':
                $scope.form_title = "Contract Service Detail";
                $http.get(API_URL + 'contractserviceModify&id=' + contractservice.cont_serv_id)
                    .then(function(response) {
                        console.log(response.data);
                        $scope.contractservice = response.data;
                    });
                break;
            default:
                break;
        }


    }

    //save new record / update existing record
    $scope.save = function(contractservice,contract_id) {
        var url = API_URL + "contractserviceSave";
        $scope.contractservice.contract_id = contract_id;
        $http({
            method: 'POST',
            url: url,
            data: jQuery.param($scope.contractservice),
            headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        }).then(function(response) {
            if(response.data.mode == 'edit'){
                angular.forEach($scope.contractservices, function(u, i) {
                    console.log(u)
                    if (u.cont_serv_id == response.data.cont_serv_id ) {
                        console.log('scope data change')
                        $scope.contractservices[i] = response.data;
                        if (!$scope.$$phase) {
                            $scope.$apply();
                        }
                        jQuery('#myFormModal').addClass('modal').removeClass('in');
                    }
                });
            }else{
                jQuery('#myFormModal').addClass('modal').removeClass('in');
                $scope.contractservices.push(response.data);
            }

        });
    }

    //delete record
    $scope.confirmDelete = function(contractservice) {
        console.log('Delete start'+contractservice.cont_serv_id);
        var contractserviceid = '';
        var isConfirmDelete = confirm('Are you sure you want this record?');
        if (isConfirmDelete) {
            $http({
                method: 'DELETE',
                url: API_URL + 'contractserviceDelete&id=' + contractservice.cont_serv_id
            }).
            then(function(response) {
                if(response.data.success == 'OK'){
                    var index = $scope.contractservices.indexOf(contractservice);
                    if (index != -1) {
                        $scope.contractservices.splice(index, 1);
                    }
                }
                else{
                    alert('Contract can not be deleted.');
                }

                //$scope.contractservices.splice($scope.contractservices.indexOf($scope.contractservice), 1);
                console.log('delete done');
                //location.reload();
            }).
            error(function(data) {
                console.log(data);
                alert('Unable to delete');
            });
        } else {
            return false;
        }
    }
    $scope.close = function(Class){
        jQuery('#myFormModal').addClass('modal').removeClass('in');
        jQuery('#myFormModal').find('form').reset();
    }
});

paymentsapp.controller('paymentsController', function($scope, $http, API_URL) {
    console.log('Initail Call is working 1');
    /*$tokenurl = window.location.href;
    $tokenurl = $tokenurl.split('=');
    //retrieve employees listing from API'
    $scope.activeData = {
        model: null,
        data: [
            {value: '1', label: 'Yes'},
            {value: '0', label: 'No'}
        ]
    };*/
    $http({
        url:API_URL + "payments",
        type:'POST'})
        .then(function(response) {
            console.log(response);
            $scope.payments = response.data.payments;
            $scope.contract = {
                model: null,
                all: response.data.contracts
            };
            $scope.paymentschedule = {
                model: null,
                all: response.data.paymentschedules
            };
            if (!$scope.$$phase) {
                $scope.$apply();
            }
        });


    //show modal form
    $scope.toggle = function(modalstate, payment) {

        $scope.modalstate = modalstate;

        console.log('working on modal state');
        switch (modalstate) {
            case 'add':
                $scope.form_title = "Add New Payment";
                $scope.payment = null;
                //console.log(payment);
                break;
            case 'edit':
                $scope.form_title = "Payment Detail";
                $http.get(API_URL + 'paymentModify&id=' + payment.pymt_id)
                    .then(function(response) {
                        console.log(response.data);
                        $scope.payment = response.data;
                    });
                break;
            default:
                break;
        }
        jQuery('#myFormModal').addClass('in').removeClass('modal');


    }

    //save new record / update existing record
    $scope.save = function(payment) {
        var url = API_URL + "paymentSave";
        //$scope.payment.contract_id = contract_id;
        $http({
            method: 'POST',
            url: url,
            data: jQuery.param($scope.payment),
            headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        }).then(function(response) {
            if(response.data.mode == 'edit'){
                angular.forEach($scope.payments, function(u, i) {
                    console.log(u)
                    if (u.pymt_id == response.data.pymt_id ) {
                        console.log('scope data change')
                        $scope.payments[i] = response.data;
                        if (!$scope.$$phase) {
                            $scope.$apply();
                        }
                        jQuery('#myFormModal').addClass('modal').removeClass('in');
                    }
                });
            }else{
                jQuery('#myFormModal').addClass('modal').removeClass('in');
                $scope.payments.push(response.data);
            }

        });
    }

    //delete record
    $scope.confirmDelete = function(payment) {
        console.log('Delete start'+payment.pymt_id);
        var paymentid = '';
        var isConfirmDelete = confirm('Are you sure you want this record?');
        if (isConfirmDelete) {
            $http({
                method: 'DELETE',
                url: API_URL + 'paymentDelete&id=' + payment.pymt_id
            }).
            then(function(response) {
                if(response.data.success == 'OK'){
                    var index = $scope.payments.indexOf(payment);
                    if (index != -1) {
                        $scope.payments.splice(index, 1);
                    }
                }
                else{
                    alert('Associated Payment Schedule Exist. Cannot be deleted.');
                }

                //$scope.payments.splice($scope.payments.indexOf($scope.payment), 1);
                console.log('delete done');
                //location.reload();
            }).
            error(function(data) {
                console.log(data);
                alert('Unable to delete');
            });
        } else {
            return false;
        }
    }
    $scope.close = function(Class){
        jQuery('#myFormModal').addClass('modal').removeClass('in');
        jQuery('#myFormModal').find('form').reset();
    }
});
