<?php

/**
 * The admin-specific functionality of the plugin.
 *
 * @link       aptech.com
 * @since      1.0.0
 *
 * @package    Contractmanager
 * @subpackage Contractmanager/admin
 */

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Contractmanager
 * @subpackage Contractmanager/admin
 * @author     Sanjay Mehra <sanjay@gmail.com>
 */
class Contractmanager_Admin {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of this plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

	}

	/**
	 * Register the stylesheets for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Contractmanager_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Contractmanager_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/contractmanager-admin.css', array(), $this->version, 'all' );
		wp_enqueue_style( 'bootstrapcss', 'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css', array(), $this->version, 'all' );
		wp_enqueue_style( 'datatablecss', 'https://cdn.datatables.net/1.10.12/css/dataTables.bootstrap.min.css', array(), $this->version, 'all' );


	}

	/**
	 * Register the JavaScript for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Contractmanager_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Contractmanager_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class. https://cdn.datatables.net/v/dt/dt-1.10.13/datatables.min.js
		 */

		wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/contractmanager-admin.js', array( 'jquery' ), $this->version, false );
		wp_enqueue_script( 'jquery', 'https://code.jquery.com/jquery-2.2.4.min.js', array(), '1.0', false );
		/*wp_enqueue_script( 'myangular', 'https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.5.5/angular.min.js', array(), '1.0', false );
		wp_enqueue_script( 'bootstraptpl', 'https://cdnjs.cloudflare.com/ajax/libs/angular-ui-bootstrap/2.0.0/ui-bootstrap-tpls.min.js', array(), '1.0', false );
		wp_enqueue_script( 'angularutil', 'https://cdnjs.cloudflare.com/ajax/libs/angular-ui-utils/0.1.1/angular-ui-utils.min.js', array(), '1.0', false );
		wp_enqueue_script( 'jquerydatatable', 'https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.12/js/jquery.dataTables.min.js', array(), '1.0', false );
		wp_enqueue_script( 'bootstrapdatatable', 'https://cdn.datatables.net/1.10.12/js/dataTables.bootstrap.min.js', array(), '1.0', false );*/






		wp_enqueue_script( 'myangular', 'https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.5.5/angular.min.js', array(), '1.0', false );
		wp_enqueue_script( 'bootstrapjs', plugin_dir_url( __FILE__ ) . 'js/app/lib/bootstrap.min.js', array(), '1.0', false );
		wp_enqueue_script( 'uibootstrap',  'https://cdnjs.cloudflare.com/ajax/libs/angular-ui-bootstrap/2.0.0/ui-bootstrap-tpls.min.js', array(), '1.0', false );
		wp_enqueue_script( 'uidatatable',  'https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.12/js/jquery.dataTables.min.js', array(), '1.0', false );
		wp_enqueue_script( 'uiutiljs',  'https://cdnjs.cloudflare.com/ajax/libs/angular-ui-utils/0.1.1/angular-ui-utils.min.js', array(), '1.0', false );
		wp_enqueue_script( 'datatablemin',  'https://cdn.datatables.net/1.10.12/js/dataTables.bootstrap.min.js', array(), '1.0', false );

		wp_enqueue_script( 'employeeapp', plugin_dir_url( __FILE__ ) . 'js/app/app.js', array(), '1.0', false );
		wp_enqueue_script( 'employeecontroller', plugin_dir_url( __FILE__ ) . 'js/app/controllers/controller.js', array(), '1.0', false );



	}

}
