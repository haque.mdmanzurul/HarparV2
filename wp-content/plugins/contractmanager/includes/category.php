<?php
/**
 * Register a custom menu page.
 */
function cm_register_categories() {
    add_submenu_page('all-contracts',
        __( 'Sevice Category', 'textdomain' ),
        'Service Category',
        'manage_options',
        'all-categories',
        'show_all_category',
        '',
        6
    );
}
add_action( 'admin_menu', 'cm_register_categories' );

/**
 * Display a custom menu page
 */
function show_all_category(){
    ob_start();
    include('templates/categoriesContainer.php');
    $output = ob_get_clean();

    echo $output;
}


add_action( 'wp_ajax_categories', 'showCategory' );
add_action( 'wp_ajax_nopriv_categories', 'showCategory' );
function showCategory(){
    global $wpdb;

    $categories = $wpdb->get_results('SELECT * FROM service_categories');

    wp_send_json(array('categories'=>$categories));
    die();
}



add_action( 'wp_ajax_categorySave', 'save_category' );
add_action( 'wp_ajax_nopriv_categorySave', 'save_category' );
function save_category(){

    global $wpdb;
    $data = $_POST;
    if($data['serv_cat_id'] !=0 or $data['serv_cat_id'] !=''){
        $cid = $data['serv_cat_id'];

        $done = $wpdb->update('service_categories',$data, array('serv_cat_id'=>$cid));
        $data['mode']='edit';

        wp_send_json($data);
    }
    else{
        $done = $wpdb->insert('service_categories',$data);
        if(!$done) $wpdb->print_error();
        $data['serv_cat_id'] = $wpdb->insert_id;
        wp_send_json($data);
    }

    die();
}


add_action( 'wp_ajax_categoryModify', 'categoryModify' );
add_action( 'wp_ajax_nopriv_categoryModify', 'categoryModify' );
function categoryModify(){
    global $wpdb;
    $category = $wpdb->get_row('SELECT * FROM service_categories WHERE serv_cat_id='.$_REQUEST['id']);

    wp_send_json($category);
    die();
}

add_action( 'wp_ajax_categoryDelete', 'categoryDelete' );
add_action( 'wp_ajax_nopriv_categoryDelete', 'categoryDelete' );
function categoryDelete(){
    global $wpdb;
    $category = $wpdb->query('DELETE FROM categories WHERE category_id='.$_GET['id']);
    //$categories = $wpdb->get_results('SELECT * FROM categories');
    wp_send_json(array('success'=>'OK','category'=>$category));
    die();
}