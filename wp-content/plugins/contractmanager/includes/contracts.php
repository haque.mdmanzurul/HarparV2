<?php
/**
 * Register a custom menu page.
 */
function cm_register_contracts() {
    add_menu_page(
        __( 'Contracts', 'textdomain' ),
        'Contracts',
        'manage_options',
        'all-contracts',
        'show_all_contract',
        '',
        6
    );
}
add_action( 'admin_menu', 'cm_register_contracts' );

/**
 * Display a custom menu page
 */
function show_all_contract(){
    global $wpdb;
    ob_start();
    if($_GET['subpage']=='')
        include('templates/contractsContainer.php');
    else{
        $contracts = $wpdb->get_row('SELECT * FROM contracts WHERE contract_id='.$_GET['contractid']);
        include('templates/contractservicesContainer.php');

    }

    $output = ob_get_clean();

    echo $output;
}


add_action( 'wp_ajax_contracts', 'showContract' );
add_action( 'wp_ajax_nopriv_contracts', 'showContract' );
function showContract(){
    global $wpdb;

    $contracts = $wpdb->get_results('SELECT * FROM contracts INNER JOIN customers ON customers.customer_id=contracts.customer_id');
    $customers = $wpdb->get_results('SELECT * FROM customers');

    wp_send_json(array('contracts'=>$contracts,'customers'=>$customers));
    die();
}



add_action( 'wp_ajax_contractSave', 'save_contract' );
add_action( 'wp_ajax_nopriv_contractSave', 'save_contract' );
function save_contract(){

    global $wpdb;
    $data = $_POST;
    if($data['contract_id'] !=0 or $data['contract_id'] !=''){
        $cid = $data['contract_id'];

        $done = $wpdb->update('contracts',$data, array('contract_id'=>$cid));
        $data['mode']='edit';
        $customer = $wpdb->get_row('SELECT * FROM customers WHERE customer_id='.$data['customer_id']);
        if($customer)
            $data['customer_name'] = $customer->customer_name;

        wp_send_json($data);
    }
    else{
        $done = $wpdb->insert('contracts',$data);
        if(!$done) $wpdb->print_error();
        $data['contract_id'] = $wpdb->insert_id;
        $customer = $wpdb->get_row('SELECT * FROM customers WHERE customer_id='.$data['customer_id']);
        if($customer)
            $data['customer_name'] = $customer->customer_name;
        wp_send_json($data);
    }

    die();
}


add_action( 'wp_ajax_contractModify', 'contractModify' );
add_action( 'wp_ajax_nopriv_contractModify', 'contractModify' );
function contractModify(){
    global $wpdb;
    $contract = $wpdb->get_row('SELECT * FROM contracts WHERE contract_id='.$_REQUEST['id']);

    wp_send_json($contract);
    die();
}

add_action( 'wp_ajax_contractDelete', 'contractDelete' );
add_action( 'wp_ajax_nopriv_contractDelete', 'contractDelete' );
function contractDelete(){
    global $wpdb;
    $result = $wpdb->query('DELETE FROM contracts WHERE contract_id='.$_REQUEST['id']);
    if($result){
        wp_send_json(array('susccess'=>'OK'));
    }
    else{
        wp_send_json(array('susccess'=>'NO'));
    }
    die();
}