<?php

/**
 * Fired during plugin activation
 *
 * @link       aptech.com
 * @since      1.0.0
 *
 * @package    Contractmanager
 * @subpackage Contractmanager/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Contractmanager
 * @subpackage Contractmanager/includes
 * @author     Sanjay Mehra <sanjay@gmail.com>
 */
class Contractmanager_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {
		global $wpdb;
		$charset_collate = $wpdb->get_charset_collate();
		require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );


		$sql = "CREATE TABLE `contracts` (
			  `contract_id` bigint(20) NOT NULL,
			  `customer_id` bigint(20) NOT NULL,
			  `contract_title` varchar(250) NOT NULL,
			  `start_dt` date DEFAULT NULL,
			  `end_dt` date DEFAULT NULL,
			  `signed_dt` date DEFAULT NULL,
			  `signed_by` varchar(250) DEFAULT NULL,
			  `contract_price` double NOT NULL
			) ENGINE=InnoDB DEFAULT CHARSET=latin1";
		dbDelta( $sql );



		$sql = "CREATE TABLE `contract_services` (
			  `cont_serv_id` bigint(20) NOT NULL,
			  `contract_id` bigint(20) NOT NULL,
			  `service_id` bigint(20) NOT NULL,
			  `staff_id` bigint(20) DEFAULT NULL,
			  `sch_date` date NOT NULL,
			  `sch_start_time` time NOT NULL,
			  `duration` int(11) NOT NULL,
			  `active` tinyint(1) NOT NULL DEFAULT '1'
			) ENGINE=InnoDB DEFAULT CHARSET=latin1;";
		dbDelta( $sql );


		$sql = "CREATE TABLE `customers` (
			  `customer_id` bigint(20) NOT NULL,
			  `customer_name` varchar(250) NOT NULL,
			  `company_name` varchar(250) DEFAULT NULL,
			  `phone_number` varchar(50) DEFAULT NULL,
			  `email_address` varchar(250) DEFAULT NULL,
			  `address` varchar(500) DEFAULT NULL,
			  `active` tinyint(1) NOT NULL DEFAULT '1',
			  `created_dt` date DEFAULT NULL
			) ENGINE=InnoDB DEFAULT CHARSET=latin1;";
		dbDelta( $sql );



		$sql = "CREATE TABLE `payments` (
			  `pymt_id` bigint(20) NOT NULL,
			  `pymt_sch_id` bigint(20) NOT NULL,
			  `pymt_amt` double NOT NULL,
			  `pymt_dt` date NOT NULL,
			  `pymt_mode` varchar(50) NOT NULL,
			  `pymt_desc` varchar(500) DEFAULT NULL
			) ENGINE=InnoDB DEFAULT CHARSET=latin1;";
		dbDelta( $sql );



		$sql = "CREATE TABLE `payment_schedule` (
			  `pymt_sch_id` bigint(20) NOT NULL,
			  `contract_id` bigint(20) NOT NULL,
			  `pymt_due` double NOT NULL,
			  `due_date` date DEFAULT NULL
			) ENGINE=InnoDB DEFAULT CHARSET=latin1;";
		dbDelta( $sql );



		$sql = "CREATE TABLE `services` (
			  `service_id` bigint(20) NOT NULL,
			  `cat_id` bigint(20) NOT NULL,
			  `serv_name` varchar(250) NOT NULL,
			  `duration` int(11) NOT NULL,
			  `price` double NOT NULL DEFAULT '0'
			) ENGINE=InnoDB DEFAULT CHARSET=latin1";
		dbDelta( $sql );


		$sql = "CREATE TABLE `service_categories` (
			  `serv_cat_id` bigint(20) NOT NULL,
			  `category_name` varchar(150) NOT NULL,
			  `active` tinyint(1) NOT NULL DEFAULT '1'
			) ENGINE=InnoDB DEFAULT CHARSET=latin1;";
		dbDelta( $sql );


		$sql = "CREATE TABLE `staff_members` (
			  `staff_id` bigint(20) NOT NULL,
			  `staff_name` varchar(250) NOT NULL,
			  `phone_number` varchar(25) DEFAULT NULL,
			  `email_address` varchar(250) DEFAULT NULL,
			  `designation` varchar(100) DEFAULT NULL,
			  `active` tinyint(1) NOT NULL DEFAULT '1'
			) ENGINE=InnoDB DEFAULT CHARSET=latin1;";
		dbDelta( $sql );


		$sql = "ALTER TABLE `contracts` ADD PRIMARY KEY (`contract_id`), ADD KEY `customer_id` (`customer_id`);";
		$wpdb->query( $sql );


		$sql = "ALTER TABLE `contract_services` ADD PRIMARY KEY (`cont_serv_id`), ADD KEY `contract_id` (`contract_id`),
  ADD KEY `service_id` (`service_id`),
  ADD KEY `staff_id` (`staff_id`);";
		$wpdb->query( $sql );


		$sql = "ALTER TABLE `customers`
  ADD PRIMARY KEY (`customer_id`);";
		$wpdb->query( $sql );


		$sql = "ALTER TABLE `payments`
  ADD PRIMARY KEY (`pymt_id`),
  ADD KEY `pymt_sch_id` (`pymt_sch_id`);";
		$wpdb->query( $sql );


		$sql = "ALTER TABLE `payment_schedule`
  ADD PRIMARY KEY (`pymt_sch_id`),
  ADD KEY `contract_id` (`contract_id`);";
		$wpdb->query( $sql );


		$sql = "ALTER TABLE `services`
  ADD PRIMARY KEY (`service_id`),
  ADD KEY `cat_id` (`cat_id`);";
		$wpdb->query( $sql );


		$sql = "ALTER TABLE `service_categories` ADD PRIMARY KEY (`serv_cat_id`);";
		$wpdb->query( $sql );


		$sql = "ALTER TABLE `staff_members` ADD PRIMARY KEY (`staff_id`);";
		$wpdb->query( $sql );


		$sql = "ALTER TABLE `contracts` MODIFY `contract_id` bigint(20) NOT NULL AUTO_INCREMENT;";
		dbDelta( $sql );

		$sql = "ALTER TABLE `contract_services` MODIFY `cont_serv_id` bigint(20) NOT NULL AUTO_INCREMENT;";
		$wpdb->query( $sql );

		$sql = " ALTER TABLE `customers` MODIFY `customer_id` bigint(20) NOT NULL AUTO_INCREMENT;";
		$wpdb->query( $sql );

		$sql = "ALTER TABLE `payments` MODIFY `pymt_id` bigint(20) NOT NULL AUTO_INCREMENT;";
		$wpdb->query( $sql );

		$sql=" ALTER TABLE `payment_schedule` MODIFY `pymt_sch_id` bigint(20) NOT NULL AUTO_INCREMENT;";
		$wpdb->query( $sql );

		$sql="ALTER TABLE `services` MODIFY `service_id` bigint(20) NOT NULL AUTO_INCREMENT;";
		$wpdb->query( $sql );

  		$sql="ALTER TABLE `service_categories` MODIFY `serv_cat_id` bigint(20) NOT NULL AUTO_INCREMENT;";
		$wpdb->query( $sql );


  		$sql="ALTER TABLE `staff_members` MODIFY `staff_id` bigint(20) NOT NULL AUTO_INCREMENT;";
		dbDelta( $sql );


		$sql="ALTER TABLE `contracts` ADD CONSTRAINT `contracts_ibfk_1` FOREIGN KEY (`customer_id`) REFERENCES `customers` (`customer_id`);";
		$wpdb->query( $sql );


		$sql="ALTER TABLE `contract_services` ADD CONSTRAINT `contract_services_ibfk_1` FOREIGN KEY (`contract_id`) REFERENCES `contracts` (`contract_id`), ADD CONSTRAINT `contract_services_ibfk_2` FOREIGN KEY (`service_id`) REFERENCES `services` (`service_id`), ADD CONSTRAINT `contract_services_ibfk_3` FOREIGN KEY (`staff_id`) REFERENCES `staff_members` (`staff_id`);";
		$wpdb->query( $sql );

		$sql="ALTER TABLE `payments` ADD CONSTRAINT `payments_ibfk_1` FOREIGN KEY (`pymt_sch_id`) REFERENCES `payments` (`pymt_id`);";
		$wpdb->query( $sql );

		$sql="ALTER TABLE `payment_schedule` ADD CONSTRAINT `payment_schedule_ibfk_1` FOREIGN KEY (`contract_id`) REFERENCES `contracts` (`contract_id`);";
		$wpdb->query( $sql );


		$sql="ALTER TABLE `services` ADD CONSTRAINT `services_ibfk_1` FOREIGN KEY (`cat_id`) REFERENCES `service_categories` (`serv_cat_id`);";
		$wpdb->query( $sql );
	}

}
