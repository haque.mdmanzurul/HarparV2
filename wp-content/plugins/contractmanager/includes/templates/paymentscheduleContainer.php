

<div class="paymentscheduleapp-container wrap" ng-app="paymentscheduleRecords">
    <div  ng-controller="paymentschedulesController">
        <h1 class="wp-heading-inline">View All Payment Schedule</h1><button id="btn-add" class="btn btn-primary btn-xs" ng-click="toggle('add', 0)">Add New</button>

        <!-- Table-to-load-the-data Part -->
        <table class="table table-bordered bordered table-striped table-condensed datatable" ui-jq="dataTable" ui-options="dataTableOpt">
            <thead>
            <tr>
                <th>ID</th>
                <th>Contract Title</th>
                <th>Amount</th>
                <th>Dute Date</th>
                <th></th>
            </tr>
            </thead>
            <tbody>
            <tr ng-repeat="(index, paymentschedule) in paymentschedules">
                <td>{{ paymentschedule.pymt_sch_id }}</td>
                <td>{{ paymentschedule.contract_title }}</td>
                <td>{{ paymentschedule.pymt_due }}</td>
                <td>{{ paymentschedule.due_date }}</td>
                <td>
                    <button class="btn btn-default btn-xs btn-detail" ng-click="toggle('edit', paymentschedule)">Edit</button>
                    <button class="btn btn-danger btn-xs btn-delete" paymentschedule_index="{{ index }}" ng-click="confirmDelete(paymentschedule)">Delete</button>
                </td>
            </tr>
            </tbody>
        </table>
        <!-- End of Table-to-load-the-data Part -->
        <!-- Modal (Pop up when detail button clicked) -->
        <div class="modal fade" id="myFormModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"  ng-click="close('close')"><span aria-hidden="true">×</span></button>
                        <h4 class="modal-title" id="myModalLabel">{{form_title}}</h4>
                    </div>
                    <div class="modal-body">
                        <form name="frmpaymentschedules" class="form-horizontal" novalidate="">
                            <input type="hidden" class="form-control" id="pymt_sch_id" name="pymt_sch_id" placeholder="Contract ID" value="{{pymt_sch_id}}"
                                   ng-model="paymentschedule.pymt_sch_id">
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-3 control-label">Contract Title</label>
                                <div class="col-sm-9">
                                    <select name="contract_id" id="contract_id" ng-model="paymentschedule.contract_id">

                                        <option ng-repeat="option in contracts.all" ng-value="option.contract_id">{{option.contract_title}}</option>
                                    </select>
                                        <span class="help-inline"
                                              ng-show="frmContracts.contract_id.$invalid && frmContracts.contract_id.$touched">Name field is required</span>
                                </div>
                            </div>


                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-3 control-label">Payment Due AMount</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control" id="pymt_due" name="title" placeholder="Payment Due Amount" value="{{pymt_due}}"
                                           ng-model="paymentschedule.pymt_due" ng-required="true">
                                        <span class="help-inline"
                                              ng-show="frmpaymentschedules.pymt_due.$invalid && frmpaymentschedules.pymt_due.$touched">Name field is required</span>
                                </div>
                            </div>



                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-3 control-label">Dute Date</label>
                                <div class="col-sm-9">
                                    <input type="datetime" id="due_date" name="due_date" ng-model="paymentschedule.due_date"
                                           placeholder="yyyy-MM-dd" min="2013-01-01" max="2013-12-31"  ng-required="true" />
                                    <!--<input type="email" class="form-control" id="email" name="email" placeholder="Email Address" value="{{email}}"
                                           ng-model="paymentschedule.email" ng-required="true">-->
                                        <span class="help-inline"
                                              ng-show="frmpaymentschedules.due_date.$invalid && frmpaymentschedules.due_date.$touched">Valid  Date is required</span>
                                </div>
                            </div>


                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-primary" id="btn-save" ng-click="save(paymentschedule)" ng-disabled="frmpaymentschedules.$invalid">Save changes</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
