

<div class="customerapp-container wrap" ng-app="customerRecords">
    <div  ng-controller="customersController">
        <h1 class="wp-heading-inline">View All Customer</h1><button id="btn-add" class="btn btn-primary btn-xs" ng-click="toggle('add', 0)">Add New Customer</button>

        <!-- Table-to-load-the-data Part -->
        <table class="table table-bordered bordered table-striped table-condensed datatable" ui-jq="dataTable" ui-options="dataTableOpt">
            <thead>
            <tr>
                <th>ID</th>
                <th>Customer Name</th>
                <th>Company Name</th>
                <th>Phone Number</th>
                <th>Email Address</th>
                <th>Address</th>
                <th>Status</th>
                <th>Created At</th>
                <th></th>
            </tr>
            </thead>
            <tbody>
            <tr ng-repeat="(index, customer) in customers">
                <td>{{ customer.customer_id }}</td>
                <td>{{ customer.customer_name }}</td>
                <td>{{ customer.company_name }}</td>
                <td>{{ customer.phone_number }}</td>
                <td>{{ customer.email_address }}</td>
                <td>{{ customer.address }}</td>
                <td>{{ customer.active }}</td>
                <td>{{ customer.created_dt }}</td>
                <td>
                    <button class="btn btn-default btn-xs btn-detail" ng-click="toggle('edit', customer)">Edit</button>
                    <button class="btn btn-danger btn-xs btn-delete" customer_index="{{ index }}" ng-click="confirmDelete(customer)">Delete</button>
                </td>
            </tr>
            </tbody>
        </table>
        <!-- End of Table-to-load-the-data Part -->
        <!-- Modal (Pop up when detail button clicked) -->
        <div class="modal fade" id="myFormModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"  ng-click="close('close')"><span aria-hidden="true">×</span></button>
                        <h4 class="modal-title" id="myModalLabel">{{form_title}}</h4>
                    </div>
                    <div class="modal-body">
                        <form name="frmCustomers" class="form-horizontal" novalidate="">
                            <input type="hidden" class="form-control" id="customer_id" name="customer_id" placeholder="Contract ID" value="{{customer_id}}"
                                   ng-model="customer.customer_id">
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-3 control-label">Customer Name</label>
                                <div class="col-sm-9">
                                <input type="text" class="form-control" id="customer_name" name="customer_name" placeholder="Customer Name" value="{{customer_name}}"
                                       ng-model="customer.customer_name" ng-required="true">
                                        <span class="help-inline"
                                              ng-show="frmCustomers.customer_name.$invalid && frmCustomers.customer_name.$touched">Name field is required</span>
                            </div>
                            </div>


                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-3 control-label">Compnay Name</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control" id="company_name" name="title" placeholder="Contract Title" value="{{company_name}}"
                                           ng-model="customer.company_name" ng-required="true">
                                        <span class="help-inline"
                                              ng-show="frmCustomers.company_name.$invalid && frmCustomers.company_name.$touched">Name field is required</span>
                                </div>
                            </div>


                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-3 control-label">Phone Number</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control" id="phone_number" name="phone_number" placeholder="Phone Number" value="{{phone_number}}"
                                           ng-model="customer.phone_number" ng-required="true">
                                        <span class="help-inline"
                                              ng-show="frmCustomers.phone_number.$invalid && frmCustomers.phone_number.$touched">Phone field is required</span>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-3 control-label">Email Address</label>
                                <div class="col-sm-9">
                                    <input type="email" class="form-control" id="email_address" name="title" placeholder="Email Address" value="{{email_address}}"
                                           ng-model="customer.email_address" ng-required="true">
                                        <span class="help-inline"
                                              ng-show="frmCustomers.email_address.$invalid && frmCustomers.email_address.$touched">Email field is required</span>
                                </div>
                            </div>


                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-3 control-label">Address</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control" id="address" name="address" placeholder=" Address" value="{{address}}"
                                           ng-model="customer.address" ng-required="true">
                                        <span class="help-inline"
                                              ng-show="frmCustomers.address.$invalid && frmCustomers.address.$touched">Address field is required</span>
                                </div>
                            </div>


                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-3 control-label">Active</label>
                                <div class="col-sm-9">
                                    <select name="active" id="active" ng-model="customer.active">

                                        <option ng-repeat="option in activeData.data" ng-value="option.value">{{option.label}}</option>
                                    </select>
                                        <span class="help-inline"
                                              ng-show="frmCustomers.customer_id.$invalid && frmCustomers.customer_id.$touched">Active field is required</span>
                                </div>
                            </div>


                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-3 control-label">Created Date</label>
                                <div class="col-sm-9">
                                    <input type="datetime" id="created_dt" name="created_dt" ng-model="customer.created_dt"
                                           placeholder="yyyy-MM-dd" min="2013-01-01" max="2013-12-31"  ng-required="true" />
                                    <!--<input type="email" class="form-control" id="email" name="email" placeholder="Email Address" value="{{email}}"
                                           ng-model="customer.email" ng-required="true">-->
                                        <span class="help-inline"
                                              ng-show="frmCustomers.created_dt.$invalid && frmCustomers.created_dt.$touched">Valid  Date is required</span>
                                </div>
                            </div>


                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-primary" id="btn-save" ng-click="save(customer)" ng-disabled="frmCustomers.$invalid">Save changes</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
