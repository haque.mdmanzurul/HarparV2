

<div class="contractserviceapp-container wrap" ng-app="contractserviceRecords">
    <div  ng-controller="contractservicesController">
        <h1 class="wp-heading-inline">View All Contract Services ( <?php echo $contracts->contract_title; ?> )</h1><button id="btn-add" class="btn btn-primary btn-xs" ng-click="toggle('add', 0)">Add New Contract Service</button>

        <!-- Table-to-load-the-data Part -->
        <table class="table table-bordered bordered table-striped table-condensed datatable" ui-jq="dataTable" ui-options="dataTableOpt">
            <thead>
            <tr>
                <th>ID</th>
                <th>Service Title</th>
                <th>Staff Name</th>
                <th>Schedule Date</th>
                <th>Schedule Start Time</th>
                <th>Duration</th>
                <th>Active</th>
                <th></th>
            </tr>
            </thead>
            <tbody>
            <tr ng-repeat="(index, contractservice) in contractservices">
                <td>{{ contractservice.cont_serv_id }}</td>
                <td>{{ contractservice.serv_name }}</td>
                <td>{{ contractservice.staff_name }}</td>
                <td>{{ contractservice.sch_date }}</td>
                <td>{{ contractservice.sch_start_time }}</td>
                <td>{{ contractservice.duration }}</td>
                <td>{{ contractservice.active }}</td>
                <td>
                    <button class="btn btn-default btn-xs btn-detail" ng-click="toggle('edit', contractservice)">Edit</button>
                    <button class="btn btn-danger btn-xs btn-delete" contractservice_index="{{ index }}" ng-click="confirmDelete(contractservice)">Delete</button>
                </td>
            </tr>
            </tbody>
        </table>
        <!-- End of Table-to-load-the-data Part -->
        <!-- Modal (Pop up when detail button clicked) -->
        <div class="modal fade" id="myFormModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close" ng-click="close('close')"><span aria-hidden="true">×</span></button>
                        <h4 class="modal-title" id="myModalLabel">{{form_title}}</h4>
                    </div>
                    <div class="modal-body">
                        <form name="frmContractservices" id="formContractservices" class="form-horizontal" novalidate="">
                            <input type="hidden" class="form-control" id="cont_serv_id" name="cont_serv_id" placeholder="Contract ID" value="{{cont_serv_id}}"
                                   ng-model="contractservice.cont_serv_id" ng-required="false">

                            <input type="hidden" class="form-control" id="contract_id" name="contract_id" value="<?php echo $contracts->contract_id; ?>" placeholder="Contract ID"
                                   ng-model="contractservice.contract_id" ng-required="false">
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-3 control-label">Staff Name</label>
                                <div class="col-sm-9">
                                    <select name="staff_id" id="staff_id" ng-model="contractservice.staff_id">

                                        <option ng-repeat="option in staff.all" ng-value="option.staff_id">{{option.staff_name}}</option>
                                    </select>
                                        <span class="help-inline"
                                              ng-show="frmContractservices.staff_id.$invalid && frmContractservices.staff_id.$touched">Staff Name field is required</span>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-3 control-label">Service Title</label>
                                <div class="col-sm-9">
                                    <select name="service_id" id="service_id" ng-model="contractservice.service_id">

                                        <option ng-repeat="option in service.all" ng-value="option.service_id">{{option.serv_name}}</option>
                                    </select>
                                        <span class="help-inline"
                                              ng-show="frmContractservices.service_id.$invalid && frmContractservices.service_id.$touched">Service Title field is required</span>
                                </div>
                            </div>

                            <!--<div class="form-group">
                                <label for="inputEmail3" class="col-sm-3 control-label">Contract Title</label>
                                <div class="col-sm-9">
                                    <select name="contract_id" id="contract_id" ng-model="contractservice.contract_id">

                                        <option ng-repeat="option in contract.all" ng-value="option.contract_id">{{option.contract_title}}</option>
                                    </select>
                                        <span class="help-inline"
                                              ng-show="frmContractservices.contract_id.$invalid && frmContractservices.contract_id.$touched">Contract Title field is required</span>
                                </div>
                            </div>-->

                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-3 control-label">Schedule Date</label>
                                <div class="col-sm-9">
                                    <input type="datetime" id="sch_date" name="sch_date" ng-model="contractservice.sch_date"
                                           placeholder="yyyy-MM-dd" min="2013-01-01" max="2013-12-31"  ng-required="true" />
                                    <!--<input type="email" class="form-control" id="email" name="email" placeholder="Email Address" value="{{email}}"
                                           ng-model="contractservice.email" ng-required="true">-->
                                        <span class="help-inline"
                                              ng-show="frmContractservices.sch_date.$invalid && frmContractservices.sch_date.$touched">Valid Schedule Date is required</span>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-3 control-label">Scheduled Start Time</label>
                                <div class="col-sm-9">
                                    <input type="datetime" id="sch_start_time" name="sch_start_time" ng-model="contractservice.sch_start_time"
                                           placeholder="yyyy-MM-dd" min="2013-01-01" max="2013-12-31"  ng-required="true" />
                                    <!--<input type="email" class="form-control" id="email" name="email" placeholder="Email Address" value="{{email}}"
                                           ng-model="contractservice.email" ng-required="true">-->
                                        <span class="help-inline"
                                              ng-show="frmContractservices.sch_start_time.$invalid && frmContractservices.sch_start_time.$touched">Valid Start Date is required</span>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-3 control-label">Duration (in minutes)</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control" id="duration" name="duration" placeholder="Duration" value="{{duration}}"
                                           ng-model="contractservice.duration" ng-required="true">
                                    <span class="help-inline"
                                          ng-show="frmContractservices.duration.$invalid && frmContractservices.duration.$touched">Duration field is required</span>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-3 control-label">Active</label>
                                <div class="col-sm-9">
                                    <select name="active" id="active" ng-model="contractservice.active">

                                        <option ng-repeat="option in activeData.data" ng-value="option.value">{{option.label}}</option>
                                    </select>
                                        <span class="help-inline"
                                              ng-show="frmCategorys.active.$invalid && frmCategorys.active.$touched">Active field is required</span>
                                </div>
                            </div>



                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-primary" id="btn-save" ng-click="save(contractservice, <?php echo $contracts->contract_id; ?>)" ng-disabled="frmContractservices.$invalid">Save changes</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
