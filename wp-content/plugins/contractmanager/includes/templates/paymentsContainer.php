

<div class="paymentapp-container wrap" ng-app="paymentRecords">
    <div  ng-controller="paymentsController">
        <h1 class="wp-heading-inline">View All Payments</h1><button id="btn-add" class="btn btn-primary btn-xs" ng-click="toggle('add', 0)">Add New Payment</button>

        <!-- Table-to-load-the-data Part -->
        <table class="table table-bordered bordered table-striped table-condensed datatable" ui-jq="dataTable" ui-options="dataTableOpt">
            <thead>
            <tr>
                <th>ID</th>
                <th>Contract Title</th>
                <th>Payment Due Date(Amount-Date)</th>
                <th>Payment Amount</th>
                <th>Date</th>
                <th>Mode</th>
                <th>Description</th>
                <th></th>
            </tr>
            </thead>
            <tbody>
            <tr ng-repeat="(index, payment) in payments">
                <td>{{ payment.pymt_id }}</td>
                <td>{{ payment.contract_title }}</td>
                <td>{{payment.pymt_due}} - {{payment.due_date}}</td>
                <td>{{ payment.pymt_amt }}</td>
                <td>{{ payment.pymt_dt }}</td>
                <td>{{ payment.pymt_mode }}</td>
                <td>{{ payment.pymt_desc }}</td>
                <td>
                    <button class="btn btn-default btn-xs btn-detail" ng-click="toggle('edit', payment)">Edit</button>
                    <button class="btn btn-danger btn-xs btn-delete" payment_index="{{ index }}" ng-click="confirmDelete(payment)">Delete</button>
                </td>
            </tr>
            </tbody>
        </table>
        <!-- End of Table-to-load-the-data Part -->
        <!-- Modal (Pop up when detail button clicked) -->
        <div class="modal fade" id="myFormModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close" ng-click="close('close')"><span aria-hidden="true">×</span></button>
                        <h4 class="modal-title" id="myModalLabel">{{form_title}}</h4>
                    </div>
                    <div class="modal-body">
                        <form name="frmPaymentservices" id="formContractservices" class="form-horizontal" novalidate="">
                            <input type="hidden" class="form-control" id="pymt_id" name="pymt_id" placeholder="Contract ID" value="{{pymt_id}}"
                                   ng-model="payment.pymt_id" ng-required="false">

                            <!--<input type="hidden" class="form-control" id="contract_id" name="contract_id" value="<?php /*echo $contracts->contract_id; */?>" placeholder="Contract ID"
                                   ng-model="payment.contract_id" ng-required="false">-->
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-3 control-label">Select Contract</label>
                                <div class="col-sm-9">
                                    <select name="contract_id" id="contract_id" ng-model="payment.contract_id">

                                        <option ng-repeat="option in contract.all" ng-value="option.contract_id">{{option.contract_title}}</option>
                                    </select>
                                        <span class="help-inline"
                                              ng-show="frmPaymentservices.contract_id.$invalid && frmPaymentservices.staff_id.$touched">Staff Name field is required</span>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-3 control-label">Payment Schedule</label>
                                <div class="col-sm-9">
                                    <select name="pymt_sch_id" id="pymt_sch_id" ng-model="payment.pymt_sch_id">

                                        <option ng-repeat="option in paymentschedule.all" ng-value="option.pymt_sch_id">{{option.pymt_due}} - {{option.due_date}}</option>
                                    </select>
                                        <span class="help-inline"
                                              ng-show="frmPaymentservices.pymt_sch_id.$invalid && frmPaymentservices.pymt_sch_id.$touched">Staff Name field is required</span>
                                </div>
                            </div>



                            <!--<div class="form-group">
                                <label for="inputEmail3" class="col-sm-3 control-label">Contract Title</label>
                                <div class="col-sm-9">
                                    <select name="contract_id" id="contract_id" ng-model="payment.contract_id">

                                        <option ng-repeat="option in contract.all" ng-value="option.contract_id">{{option.contract_title}}</option>
                                    </select>
                                        <span class="help-inline"
                                              ng-show="frmPaymentservices.contract_id.$invalid && frmPaymentservices.contract_id.$touched">Contract Title field is required</span>
                                </div>
                            </div>-->





                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-3 control-label">Payment Amount</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control" id="pymt_amt" name="pymt_amt" placeholder="Payment Amount" value="{{duration}}"
                                           ng-model="payment.pymt_amt" ng-required="true">
                                    <span class="help-inline"
                                          ng-show="frmPaymentservices.pymt_amt.$invalid && frmPaymentservices.pymt_amt.$touched">Amount field is required</span>
                                </div>
                            </div>


                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-3 control-label">Payment Date</label>
                                <div class="col-sm-9">
                                    <input type="datetime" id="pymt_dt" name="pymt_dt" ng-model="payment.pymt_dt"
                                           placeholder="yyyy-MM-dd" min="2013-01-01" max="2013-12-31"  ng-required="true" />
                                    <!--<input type="email" class="form-control" id="email" name="email" placeholder="Email Address" value="{{email}}"
                                           ng-model="payment.email" ng-required="true">-->
                                        <span class="help-inline"
                                              ng-show="frmPaymentservices.pymt_dt.$invalid && frmPaymentservices.pymt_dt.$touched">Payment Date is required</span>
                                </div>
                            </div>


                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-3 control-label">Payment Mode</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control" id="pymt_mode" name="pymt_mode" placeholder="Payment Mode" value="{{pymt_mode}}"
                                           ng-model="payment.pymt_mode" ng-required="false">
                                    <span class="help-inline"
                                          ng-show="frmPaymentservices.pymt_mode.$invalid && frmPaymentservices.pymt_mode.$touched">Duration field is required</span>
                                </div>
                            </div>


                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-3 control-label">Payment Description</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control" id="pymt_desc" name="pymt_desc" placeholder="Payment Description" value="{{duration}}"
                                           ng-model="payment.pymt_desc" ng-required="false">
                                    <span class="help-inline"
                                          ng-show="frmPaymentservices.pymt_desc.$invalid && frmPaymentservices.pymt_desc.$touched">Duration field is required</span>
                                </div>
                            </div>





                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-primary" id="btn-save" ng-click="save(payment)" ng-disabled="frmPaymentservices.$invalid">Save changes</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
