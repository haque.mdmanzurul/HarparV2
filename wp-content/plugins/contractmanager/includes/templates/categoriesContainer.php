

<div class="categoryapp-container wrap" ng-app="categoryRecords">
    <div  ng-controller="categoriesController">
        <h1 class="wp-heading-inline">View All Category</h1><button id="btn-add" class="btn btn-primary btn-xs" ng-click="toggle('add', 0)">Add New Category</button>

        <!-- Table-to-load-the-data Part -->
        <table class="table table-bordered bordered table-striped table-condensed datatable" ui-jq="dataTable" ui-options="dataTableOpt">
            <thead>
            <tr>
                <th>ID</th>
                <th>Category Name</th>
                <th>Active</th>
                <th></th>
            </tr>
            </thead>
            <tbody>
            <tr ng-repeat="(index, category) in categories">
                <td>{{ category.serv_cat_id }}</td>
                <td>{{ category.category_name }}</td>
                <td>

                    <div ng-switch="category.active">
                        <span ng-switch-when="1">Yes</span>
                        <span ng-switch-when="0">No</span>
                        <span ng-switch-default>Yes</span>
                    </div>
                </td>
                <td>
                    <button class="btn btn-default btn-xs btn-detail" ng-click="toggle('edit', category)">Edit</button>
                    <button class="btn btn-danger btn-xs btn-delete" category_index="{{ index }}" ng-click="confirmDelete(category)">Delete</button>
                </td>
            </tr>
            </tbody>
        </table>
        <!-- End of Table-to-load-the-data Part -->
        <!-- Modal (Pop up when detail button clicked) -->
        <div class="modal fade" id="myFormModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"  ng-click="close('close')"><span aria-hidden="true">×</span></button>
                        <h4 class="modal-title" id="myModalLabel">{{form_title}}</h4>
                    </div>
                    <div class="modal-body">
                        <form name="frmCategorys" class="form-horizontal" novalidate="">
                            <input type="hidden" class="form-control" id="serv_cat_id" name="serv_cat_id" placeholder="category ID" value="{{serv_cat_id}}"
                                   ng-model="category.serv_cat_id">
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-3 control-label">Category Name</label>
                                <div class="col-sm-9">
                                <input type="text" class="form-control" id="category_name" name="category_name" placeholder="Category Name" value="{{category_name}}"
                                       ng-model="category.category_name" ng-required="true">
                                        <span class="help-inline"
                                              ng-show="frmCategorys.category_name.$invalid && frmCategorys.category_name.$touched">Name field is required</span>
                            </div>
                            </div>

                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-3 control-label">Active</label>
                                <div class="col-sm-9">
                                    <select name="active" id="active" ng-model="category.active">

                                        <option ng-repeat="option in activeData.data" ng-value="option.value">{{option.label}}</option>
                                    </select>
                                        <span class="help-inline"
                                              ng-show="frmCategorys.active.$invalid && frmCategorys.active.$touched">Active field is required</span>
                                </div>
                            </div>

                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-primary" id="btn-save" ng-click="save(category)" ng-disabled="frmCategorys.$invalid">Save changes</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
