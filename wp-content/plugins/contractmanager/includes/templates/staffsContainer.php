

<div class="staffapp-container wrap" ng-app="staffRecords">
    <div  ng-controller="staffsController">
        <h1 class="wp-heading-inline">View All Staff</h1><button id="btn-add" class="btn btn-primary btn-xs" ng-click="toggle('add', 0)">Add New Staff</button>

        <!-- Table-to-load-the-data Part -->
        <table class="table">
            <thead>
            <tr>
                <th>ID</th>
                <th>Staff Name</th>
                <th>Phone Number</th>
                <th>Email Address</th>
                <th>Address</th>
                <th>Status</th>
                <th></th>
            </tr>
            </thead>
            <tbody>
            <tr ng-repeat="(index, staff) in staffs">
                <td>{{ staff.staff_id }}</td>
                <td>{{ staff.staff_name }}</td>
                <td>{{ staff.phone_number }}</td>
                <td>{{ staff.email_address }}</td>
                <td>{{ staff.designation }}</td>
                <td>{{ staff.active }}</td>
                <td>
                    <button class="btn btn-default btn-xs btn-detail" ng-click="toggle('edit', staff)">Edit</button>
                    <button class="btn btn-danger btn-xs btn-delete" staff_index="{{ index }}" ng-click="confirmDelete(staff)">Delete</button>
                </td>
            </tr>
            </tbody>
        </table>
        <!-- End of Table-to-load-the-data Part -->
        <!-- Modal (Pop up when detail button clicked) -->
        <div class="modal fade" id="myFormModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"  ng-click="close('close')"><span aria-hidden="true">×</span></button>
                        <h4 class="modal-title" id="myModalLabel">{{form_title}}</h4>
                    </div>
                    <div class="modal-body">
                        <form name="frmStaffs" class="form-horizontal" novalidate="">
                            <input type="hidden" class="form-control" id="staff_id" name="staff_id" placeholder="Contract ID" value="{{staff_id}}"
                                   ng-model="staff.staff_id">
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-3 control-label">Staff Name</label>
                                <div class="col-sm-9">
                                <input type="text" class="form-control" id="staff_name" name="staff_name" placeholder="Staff Name" value="{{staff_name}}"
                                       ng-model="staff.staff_name" ng-required="true">
                                        <span class="help-inline"
                                              ng-show="frmStaffs.staff_name.$invalid && frmStaffs.staff_name.$touched">Name field is required</span>
                            </div>
                            </div>





                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-3 control-label">Phone Number</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control" id="phone_number" name="phone_number" placeholder="Phone Number" value="{{phone_number}}"
                                           ng-model="staff.phone_number" ng-required="true">
                                        <span class="help-inline"
                                              ng-show="frmStaffs.phone_number.$invalid && frmStaffs.phone_number.$touched">Phone field is required</span>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-3 control-label">Email Address</label>
                                <div class="col-sm-9">
                                    <input type="email" class="form-control" id="email_address" name="title" placeholder="Email Address" value="{{email_address}}"
                                           ng-model="staff.email_address" ng-required="true">
                                        <span class="help-inline"
                                              ng-show="frmStaffs.email_address.$invalid && frmStaffs.email_address.$touched">Email field is required</span>
                                </div>
                            </div>


                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-3 control-label">Designation</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control" id="designation" name="designation" placeholder="Designation" value="{{designation}}"
                                           ng-model="staff.designation" ng-required="true">
                                        <span class="help-inline"
                                              ng-show="frmStaffs.designation.$invalid && frmStaffs.designation.$touched">Designation field is required</span>
                                </div>
                            </div>


                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-3 control-label">Active</label>
                                <div class="col-sm-9">
                                    <select name="active" id="active" ng-model="staff.active">

                                        <option ng-repeat="option in activeData.data" ng-value="option.value">{{option.label}}</option>
                                    </select>
                                        <span class="help-inline"
                                              ng-show="frmStaffs.staff_id.$invalid && frmStaffs.staff_id.$touched">Active field is required</span>
                                </div>
                            </div>





                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-primary" id="btn-save" ng-click="save(staff)" ng-disabled="frmStaffs.$invalid">Save changes</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
