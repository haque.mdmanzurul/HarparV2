

<div class="contractapp-container wrap" ng-app="contractRecords">
    <div  data-ng-controller="contractsController">
        <h1 class="wp-heading-inline">View All Contracts</h1><button id="btn-add" class="btn btn-primary btn-xs" ng-click="toggle('add', 0)">Add New Contract</button>

        <!-- Table-to-load-the-data Part -->
        <!--<table id="contractsTable" class="table">-->
        <table id="contractsTable" class="table table-bordered bordered table-striped table-condensed datatable">

            <thead>
            <tr>
                <th>ID</th>
                <th>Customer Name</th>
                <th>Title</th>
                <th>Amount</th>
                <th>Start Date</th>
                <th>End Date</th>
                <th>Signed Date</th>
                <th>Signed By</th>
                <th>Action</th
            </tr>
            </thead>
            <tbody>
            <tr ng-repeat="(index, contract) in contracts">
                <td>{{ contract.contract_id }}</td>
                <td>{{ contract.customer_name }}</td>
                <td>{{ contract.contract_title }}</td>
                <td>{{ contract.contract_price }}</td>
                <td>{{ contract.start_dt }}</td>
                <td>{{ contract.end_dt }}</td>
                <td>{{ contract.signed_dt }}</td>
                <td>{{ contract.signed_by }}</td>

                <td>
                    <a href="admin.php?page=all-contracts&subpage=manageservices&contractid={{ contract.contract_id }}" class="button button-small">Manage Services</a>
                    <a href="#" class="button button-small" ng-click="toggle('edit', contract)">Edit</a>
                    <a href="#" class="button button-small btn-danger" contract_index="{{ index }}" ng-click="confirmDelete(contract)">Delete</a>
                    <!--<button class="btn btn-default btn-xs btn-detail" ng-click="toggle('edit', contract)">Edit</button>
                    <button class="btn btn-danger btn-xs btn-delete" contract_index="{{ index }}" ng-click="confirmDelete(contract)">Delete</button>-->
                </td>
            </tr>
            </tbody>
        </table>
        <!-- End of Table-to-load-the-data Part -->
        <!-- Modal (Pop up when detail button clicked) -->
        <div class="modal fade" id="myFormModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close" ng-click="close('close')"><span aria-hidden="true">×</span></button>
                        <h4 class="modal-title" id="myModalLabel">{{form_title}}</h4>
                    </div>
                    <div class="modal-body">
                        <form name="frmContracts" class="form-horizontal" novalidate="">
                            <input type="hidden" class="form-control" id="contract_id" name="contract_id" placeholder="Contract ID" value="{{contract_id}}"
                                   ng-model="contract.contract_id" ng-required="false">
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-3 control-label">Customer Name</label>
                                <div class="col-sm-9">
                                    <select name="customer_id" id="customer_id" ng-model="contract.customer_id">

                                        <option ng-repeat="option in customer.all" ng-value="option.customer_id">{{option.customer_name}}</option>
                                    </select>
                                        <span class="help-inline"
                                              ng-show="frmContracts.customer_id.$invalid && frmContracts.customer_id.$touched">Name field is required</span>
                                </div>
                            </div>


                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-3 control-label">Contract Title</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control" id="contract_title" name="title" placeholder="Contract Title" value="{{contract_title}}"
                                           ng-model="contract.contract_title" ng-required="true">
                                        <span class="help-inline"
                                              ng-show="frmContracts.contract_title.$invalid && frmContracts.contract_title.$touched">Name field is required</span>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-3 control-label">Start Date</label>
                                <div class="col-sm-9">
                                    <input type="datetime" id="start_dt" name="start_dt" ng-model="contract.start_dt"
                                           placeholder="yyyy-MM-dd" min="2013-01-01" max="2013-12-31"  ng-required="true" />
                                    <!--<input type="email" class="form-control" id="email" name="email" placeholder="Email Address" value="{{email}}"
                                           ng-model="contract.email" ng-required="true">-->
                                        <span class="help-inline"
                                              ng-show="frmContracts.start_dt.$invalid && frmContracts.start_dt.$touched">Valid Start Date is required</span>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-3 control-label">End Date</label>
                                <div class="col-sm-9">
                                    <input type="datetime" id="end_dt" name="end_dt" ng-model="contract.end_dt"
                                           placeholder="yyyy-MM-dd" min="2013-01-01" max="2013-12-31"  ng-required="true" />
                                    <!--<input type="email" class="form-control" id="email" name="email" placeholder="Email Address" value="{{email}}"
                                           ng-model="contract.email" ng-required="true">-->
                                        <span class="help-inline"
                                              ng-show="frmContracts.end_dt.$invalid && frmContracts.end_dt.$touched">Valid Start Date is required</span>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-3 control-label">Signed Date</label>
                                <div class="col-sm-9">
                                    <input type="datetime" id="signed_dt" name="signed_dt" ng-model="contract.signed_dt"
                                           placeholder="yyyy-MM-dd" min="2013-01-01" max="2013-12-31"  ng-required="true" />
                                    <!--<input type="email" class="form-control" id="email" name="email" placeholder="Email Address" value="{{email}}"
                                           ng-model="contract.email" ng-required="true">-->
                                        <span class="help-inline"
                                              ng-show="frmContracts.signed_dt.$invalid && frmContracts.signed_dt.$touched">Valid  Date is required</span>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-3 control-label">Signed By</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control" id="signed_by" name="signed_by" placeholder="Signed By" value="{{signed_by}}"
                                           ng-model="contract.signed_by" ng-required="true">
                                    <span class="help-inline"
                                          ng-show="frmContracts.signed_by.$invalid && frmContracts.signed_by.$touched">Signed By field is required</span>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-3 control-label">Contract Price</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control" id="contract_price" name="contract_price" placeholder="Contact Price" value="{{contract_price}}"
                                           ng-model="contract.contract_price" ng-required="true">
                                    <span class="help-inline"
                                          ng-show="frmContracts.contract_price.$invalid && frmContracts.contract_price.$touched">Contact number field is required</span>
                                </div>
                            </div>



                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-primary" id="btn-save" ng-click="save(contract)" ng-disabled="frmContracts.$invalid">Save changes</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
