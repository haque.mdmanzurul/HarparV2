

<div class="serviceapp-container wrap" ng-app="serviceRecords">
    <div  ng-controller="servicesController">
        <h1 class="wp-heading-inline">View All Services</h1><button id="btn-add" class="btn btn-primary btn-xs" ng-click="toggle('add', 0)">Add New Service</button>

        <!-- Table-to-load-the-data Part -->
        <table class="table table-bordered bordered table-striped table-condensed datatable" ui-jq="dataTable" ui-options="dataTableOpt">
            <thead>
            <tr>
                <th>ID</th>
                <th>Service Name</th>
                <th>Duration</th>
                <th>Price</th>
                <th></th>
            </tr>
            </thead>
            <tbody>
            <tr ng-repeat="(index, service) in services">
                <td>{{ service.service_id }}</td>
                <td>{{ service.serv_name }}</td>
                <td>{{ service.duration }}</td>
                <td>{{ service.price }}</td>
                <td>
                    <button class="btn btn-default btn-xs btn-detail" ng-click="toggle('edit', service)">Edit</button>
                    <button class="btn btn-danger btn-xs btn-delete" service_index="{{ index }}" ng-click="confirmDelete(service)">Delete</button>
                </td>
            </tr>
            </tbody>
        </table>
        <!-- End of Table-to-load-the-data Part -->
        <!-- Modal (Pop up when detail button clicked) -->
        <div class="modal fade" id="myFormModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"  ng-click="close('close')"><span aria-hidden="true">×</span></button>
                        <h4 class="modal-title" id="myModalLabel">{{form_title}}</h4>
                    </div>
                    <div class="modal-body">
                        <form name="frmServices" class="form-horizontal" novalidate="">
                            <input type="hidden" class="form-control" id="service_id" name="service_id" placeholder="Service ID" value="{{service_id}}"
                                   ng-model="service.service_id" ng-required="false">
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-3 control-label">Service Category Name</label>
                                <div class="col-sm-9">
                                    <select name="cat_id" id="cat_id" ng-model="service.cat_id">

                                        <option ng-repeat="option in servicecategory.all" ng-value="option.serv_cat_id">{{option.category_name}}</option>
                                    </select>
                                        <span class="help-inline"
                                              ng-show="frmServices.cat_id.$invalid && frmServices.cat_id.$touched">Service Category Needed field is required</span>
                                </div>
                            </div>


                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-3 control-label">Service Name</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control" id="serv_name" name="Service Name" placeholder="Service Name" value="{{serv_name}}"
                                           ng-model="service.serv_name" ng-required="true">
                                        <span class="help-inline"
                                              ng-show="frmServices.serv_name.$invalid && frmServices.serv_name.$touched">Name field is required</span>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-3 control-label">Duration</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control" id="duration" name="duration" placeholder="Duration in days" value="{{duration}}"
                                           ng-model="service.duration" ng-required="true">
                                    <span class="help-inline"
                                          ng-show="frmServices.duration.$invalid && frmServices.duration.$touched">Duration field is required</span>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-3 control-label">Price</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control" id="price" name="price" placeholder=" Price" value="{{service_price}}"
                                           ng-model="service.price" ng-required="true">
                                    <span class="help-inline"
                                          ng-show="frmServices.price.$invalid && frmServices.price.$touched">Price field is required</span>
                                </div>
                            </div>



                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-primary" id="btn-save" ng-click="save(service)" ng-disabled="frmServices.$invalid">Save changes</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
