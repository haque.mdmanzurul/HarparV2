<?php
/**
 * Register a custom menu page.
 */
function cm_register_payments() {
    add_submenu_page('all-contracts',
        __( 'Payments', 'textdomain' ),
        'Payments',
        'manage_options',
        'all-payments',
        'show_all_payment',
        '',
        6
    );
}
add_action( 'admin_menu', 'cm_register_payments' );

/**
 * Display a custom menu page
 */
function show_all_payment(){
    global $wpdb;
    ob_start();
    $contracts = $wpdb->get_row('SELECT * FROM contracts WHERE contract_id='.$_GET['contractid']);
    include('templates/paymentsContainer.php');
    $output = ob_get_clean();

    echo $output;
}


add_action( 'wp_ajax_payments', 'showPayment' );
add_action( 'wp_ajax_nopriv_payments', 'showPayment' );
function showPayment(){
    global $wpdb;

    $cid = $_GET['contractid'];

    $payments = $wpdb->get_results('SELECT payments.*, payment_schedule.pymt_sch_id,payment_schedule.pymt_due,payment_schedule.due_date, contracts.contract_id, contracts.contract_title FROM payments INNER JOIN payment_schedule ON payment_schedule.pymt_sch_id=payments.pymt_sch_id INNER JOIN contracts  ON contracts.contract_id=payment_schedule.contract_id');
    $paymentschedules = $wpdb->get_results('SELECT * FROM payment_schedule');
    $contracts = $wpdb->get_results('SELECT * FROM contracts');

    wp_send_json(array('payments'=>$payments,'paymentschedules'=>$paymentschedules,'contracts'=>$contracts));
    die();
}



add_action( 'wp_ajax_paymentSave', 'save_payment' );
add_action( 'wp_ajax_nopriv_paymentSave', 'save_payment' );
function save_payment(){

    global $wpdb;
    $data = $_POST;
    if($data['pymt_id'] !=0 or $data['pymt_id'] !=''){
        $pymt_id = $data['pymt_id'];
        $savedata = array();
        $savedata['pymt_sch_id'] = $data['pymt_sch_id'];
        $savedata['pymt_amt'] = $data['pymt_amt'];
        $savedata['pymt_desc'] = $data['pymt_desc'];
        $savedata['pymt_dt'] = $data['pymt_dt'];
        $savedata['pymt_mode'] = $data['pymt_mode'];
        $done = $wpdb->update('payments',$savedata, array('pymt_id'=>$pymt_id));
        $data['mode']='edit';

        $payment_schedule = $wpdb->get_row('SELECT * FROM payment_schedule WHERE pymt_sch_id='.$data['pymt_sch_id']);
        if($payment_schedule){
            $data['pymt_due'] = $payment_schedule->pymt_due;
            $data['due_date'] = $payment_schedule->due_date;
        }


        $contract = $wpdb->get_row('SELECT * FROM contracts WHERE contract_id='.$data['contract_id']);
        if($contract)
            $data['contract_title'] = $contract->contract_title;

        wp_send_json($data);
    }
    else{
        $savedata = array();
        $savedata['pymt_sch_id'] = $data['pymt_sch_id'];
        $savedata['pymt_amt'] = $data['pymt_amt'];
        $savedata['pymt_desc'] = $data['pymt_desc'];
        $savedata['pymt_dt'] = $data['pymt_dt'];
        $savedata['pymt_mode'] = $data['pymt_mode'];

        $done = $wpdb->insert('payments',$savedata);
        if(!$done) wp_send_json($wpdb->print_error());;
        $data['pymt_id'] = $wpdb->insert_id;

        $payment_schedule = $wpdb->get_row('SELECT * FROM payment_schedule WHERE pymt_sch_id='.$data['pymt_sch_id']);
        if($payment_schedule){
            $data['pymt_due'] = $payment_schedule->pymt_due;
            $data['due_date'] = $payment_schedule->due_date;
        }


        $contract = $wpdb->get_row('SELECT * FROM contracts WHERE contract_id='.$data['contract_id']);
        if($contract)
            $data['contract_title'] = $contract->contract_title;

        if($data['pymt_id'] == 0)
            wp_send_json('error');
        else
        wp_send_json($data);
    }

    die();
}


add_action( 'wp_ajax_paymentModify', 'paymentModify' );
add_action( 'wp_ajax_nopriv_paymentModify', 'paymentModify' );
function paymentModify(){
    global $wpdb;
    $payment = $wpdb->get_row('SELECT payments.*, payment_schedule.pymt_sch_id,payment_schedule.pymt_due,payment_schedule.due_date, contracts.contract_id, contracts.contract_title FROM payments INNER JOIN payment_schedule ON payment_schedule.pymt_sch_id=payments.pymt_sch_id INNER JOIN contracts  ON contracts.contract_id=payment_schedule.contract_id WHERE pymt_id='.$_REQUEST['id']);

    wp_send_json($payment);
    die();
}

add_action( 'wp_ajax_paymentDelete', 'paymentDelete' );
add_action( 'wp_ajax_nopriv_paymentDelete', 'paymentDelete' );
function paymentDelete(){
    global $wpdb;
    $payment = $wpdb->get_row('SELECT * FROM payments WHERE pymt_id='.$_REQUEST['id']);

    $paymentschedule = $wpdb->get_row('SELECT * FROM payment_schedule WHERE pymt_sch_id='.$payment->pymt_sch_id);

    if(count($paymentschedule)>0)
        wp_send_json(array('susccess'=>'NO'));

    $result = $wpdb->query('DELETE FROM payments WHERE cont_serv_id='.$_REQUEST['id']);
    if($result){
        wp_send_json(array('susccess'=>'OK'));
    }
    else{
        wp_send_json(array('susccess'=>'NO'));
    }
    die();
}