<?php

/**
 * Fired during plugin deactivation
 *
 * @link       aptech.com
 * @since      1.0.0
 *
 * @package    Contractmanager
 * @subpackage Contractmanager/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Contractmanager
 * @subpackage Contractmanager/includes
 * @author     Sanjay Mehra <sanjay@gmail.com>
 */
class Contractmanager_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {

	}

}
