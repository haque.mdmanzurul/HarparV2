<?php
/**
 * Register a custom menu page.
 */
function cm_register_contractservices() {
    add_submenu_page('all-contracts',
        __( 'Contract Services', 'textdomain' ),
        'Contract Services',
        'manage_options',
        'all-contractservices',
        'show_all_contractservice',
        '',
        6
    );
}
//add_action( 'admin_menu', 'cm_register_contractservices' );

/**
 * Display a custom menu page
 */
function show_all_contractservice(){
    ob_start();
    include('templates/contractservicesContainer.php');
    $output = ob_get_clean();

    echo $output;
}


add_action( 'wp_ajax_contractservices', 'showContractservice' );
add_action( 'wp_ajax_nopriv_contractservices', 'showContractservice' );
function showContractservice(){
    global $wpdb;

    $cid = $_GET['contractid'];

    $contractservices = $wpdb->get_results('SELECT contract_services.*, staff_members.staff_id, staff_members.staff_name,  services.service_id, services.serv_name, contracts.contract_id, contracts.contract_title FROM contract_services INNER JOIN staff_members ON staff_members.staff_id=contract_services.staff_id INNER JOIN services ON services.service_id=contract_services.service_id INNER JOIN contracts  ON contracts.contract_id=contract_services.contract_id WHERE contract_services.contract_id='.$cid);
    $staffs = $wpdb->get_results('SELECT * FROM staff_members');
    $services = $wpdb->get_results('SELECT * FROM services');
    $contracts = $wpdb->get_results('SELECT * FROM contracts');

    wp_send_json(array('contractservices'=>$contractservices,'staffs'=>$staffs,'services'=>$services,'contracts'=>$contracts));
    die();
}



add_action( 'wp_ajax_contractserviceSave', 'save_contractservice' );
add_action( 'wp_ajax_nopriv_contractserviceSave', 'save_contractservice' );
function save_contractservice(){

    global $wpdb;
    $data = $_POST;
    if($data['cont_serv_id'] !=0 or $data['cont_serv_id'] !=''){
        $cid = $data['cont_serv_id'];
        $savedata = array();
        $savedata['contract_id'] = $data['contract_id'];
        $savedata['service_id'] = $data['service_id'];
        $savedata['staff_id'] = $data['staff_id'];
        $savedata['duration'] = $data['duration'];
        $savedata['active'] = $data['active'];
        $savedata['sch_date'] = $data['sch_date'];
        $savedata['sch_start_time'] = $data['sch_start_time'];
        $done = $wpdb->update('contract_services',$savedata, array('cont_serv_id'=>$cid));
        $data['mode']='edit';

        $staff = $wpdb->get_row('SELECT * FROM staff_members WHERE staff_id='.$data['staff_id']);
        if($staff)
            $data['staff_name'] = $staff->staff_name;

        $contract = $wpdb->get_row('SELECT * FROM contracts WHERE contract_id='.$data['contract_id']);
        if($contract)
            $data['contract_title'] = $contract->contract_title;

        $service = $wpdb->get_row('SELECT * FROM services WHERE service_id='.$data['service_id']);
        if($service)
            $data['serv_name'] = $service->serv_name;

        wp_send_json($data);
    }
    else{
        $done = $wpdb->insert('contract_services',$data);
        if(!$done) $wpdb->print_error();
        $data['cont_serv_id'] = $wpdb->insert_id;

        $staff = $wpdb->get_row('SELECT * FROM staff_members WHERE staff_id='.$data['staff_id']);
        if($staff)
            $data['staff_name'] = $staff->staff_name;

        $contract = $wpdb->get_row('SELECT * FROM contracts WHERE contract_id='.$data['contract_id']);
        if($contract)
            $data['contract_title'] = $contract->contract_title;

        $service = $wpdb->get_row('SELECT * FROM services WHERE service_id='.$data['service_id']);
        if($service)
            $data['serv_name'] = $service->serv_name;
        wp_send_json($data);
    }

    die();
}


add_action( 'wp_ajax_contractserviceModify', 'contractserviceModify' );
add_action( 'wp_ajax_nopriv_contractserviceModify', 'contractserviceModify' );
function contractserviceModify(){
    global $wpdb;
    $contractservice = $wpdb->get_row('SELECT contract_services.*, staff_members.staff_id, staff_members.staff_name,  services.service_id, services.serv_name, contracts.contract_id, contracts.contract_title FROM contract_services INNER JOIN staff_members ON staff_members.staff_id=contract_services.staff_id INNER JOIN services ON services.service_id=contract_services.service_id INNER JOIN contracts  ON contracts.contract_id=contract_services.contract_id WHERE cont_serv_id='.$_REQUEST['id']);

    wp_send_json($contractservice);
    die();
}

add_action( 'wp_ajax_contractserviceDelete', 'contractserviceDelete' );
add_action( 'wp_ajax_nopriv_contractserviceDelete', 'contractserviceDelete' );
function contractserviceDelete(){
    global $wpdb;
    $result = $wpdb->query('DELETE FROM contract_services WHERE cont_serv_id='.$_REQUEST['id']);
    if($result){
        wp_send_json(array('susccess'=>'OK'));
    }
    else{
        wp_send_json(array('susccess'=>'NO'));
    }
    die();
}