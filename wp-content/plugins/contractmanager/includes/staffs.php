<?php
/**
 * Register a custom menu page.
 */
function cm_register_staffs() {
    add_submenu_page('all-contracts',
        __( 'Staffs', 'textdomain' ),
        'Staffs',
        'manage_options',
        'all-staffs',
        'show_all_staff',
        '',
        6
    );
}
add_action( 'admin_menu', 'cm_register_staffs' );

/**
 * Display a custom menu page
 */
function show_all_staff(){
    ob_start();
    include('templates/staffsContainer.php');
    $output = ob_get_clean();

    echo $output;
}


add_action( 'wp_ajax_staffs', 'showStaff' );
add_action( 'wp_ajax_nopriv_staffs', 'showStaff' );
function showStaff(){
    global $wpdb;

    $staffs = $wpdb->get_results('SELECT * FROM staff_members');

    wp_send_json(array('staffs'=>$staffs));
    die();
}



add_action( 'wp_ajax_staffSave', 'save_staff' );
add_action( 'wp_ajax_nopriv_staffSave', 'save_staff' );
function save_staff(){

    global $wpdb;
    $data = $_POST;
    if($data['staff_id'] !=0 or $data['staff_id'] !=''){
        $cid = $data['staff_id'];

        $done = $wpdb->update('staff_members',$data, array('staff_id'=>$cid));
        $data['mode']='edit';

        wp_send_json($data);
    }
    else{
        $done = $wpdb->insert('staff_members',$data);
        if(!$done) $wpdb->print_error();
        $data['staff_id'] = $wpdb->insert_id;
        wp_send_json($data);
    }

    die();
}


add_action( 'wp_ajax_staffModify', 'staffModify' );
add_action( 'wp_ajax_nopriv_staffModify', 'staffModify' );
function staffModify(){
    global $wpdb;
    $staff = $wpdb->get_row('SELECT * FROM staff_members WHERE staff_id='.$_REQUEST['id']);

    wp_send_json($staff);
    die();
}

add_action( 'wp_ajax_staffDelete', 'staffDelete' );
add_action( 'wp_ajax_nopriv_staffDelete', 'staffDelete' );
function staffDelete(){
    global $wpdb;
    $staff = $wpdb->query('DELETE FROM staff_members WHERE staff_id='.$_GET['id']);
    wp_send_json(array('success'=>'OK','staff'=>$staff));
    die();
}