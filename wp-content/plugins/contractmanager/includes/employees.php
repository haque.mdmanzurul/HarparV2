<?php
/**
 * Register a custom menu page.
 */
function cm_register_my_custom_menu_page() {
    add_menu_page(
        __( 'Contract Manager', 'textdomain' ),
        'Contract Manager',
        'manage_options',
        'all-contracts',
        'show_all_contract',
        '',
        6
    );
}
add_action( 'admin_menu', 'cm_register_my_custom_menu_page' );

/**
 * Display a custom menu page
 */
function show_all_contract(){
    ob_start();
    include('templates/contractsContainer.php');
    $output = ob_get_clean();

    echo $output;
}


add_action( 'wp_ajax_employees', 'showemployee' );
add_action( 'wp_ajax_nopriv_employees', 'showemployee' );
function showemployee(){
    $employees = array(
        array('id' => '1','name' => 'Manzurul','email' => 'md@gmail.com','contact_number' => '234234','position' => 'Developer'),
        array('id' => '2','name' => 'Babul','email' => 'md@gmail.com','contact_number' => '234234','position' => 'Developer')
    );

    wp_send_json($employees);
    die();
}



add_action( 'wp_ajax_employeeSave', 'save_employee' );
add_action( 'wp_ajax_nopriv_employeeSave', 'save_employee' );
function save_employee(){
    $employees = array(
        array('id' => '3','name' => 'Babul New','email' => 'nnnmd@gmail.com','contact_number' => '234234','position' => 'Developer')
    );

    wp_send_json(array('id' => '3','name' => 'Babul New','email' => 'nnnmd@gmail.com','contact_number' => '234234','position' => 'Developer'));
    die();
}


add_action( 'wp_ajax_employeeModify', 'employeeModify' );
add_action( 'wp_ajax_nopriv_employeeModify', 'employeeModify' );
function employeeModify(){
    $employees = array(
        array('id' => '2','name' => 'Babul','email' => 'md@gmail.com','contact_number' => '234234','position' => 'Developer')
    );

    wp_send_json(array('id' => '2','name' => 'Babul','email' => 'md@gmail.com','contact_number' => '234234','position' => 'Developer'));
    die();
}