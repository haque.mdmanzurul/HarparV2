<?php
/**
 * Register a custom menu page.
 */
function cm_register_customers() {
    add_submenu_page('all-contracts',
        __( 'Customers', 'textdomain' ),
        'Customers',
        'manage_options',
        'all-customers',
        'show_all_customer',
        '',
        6
    );
}
add_action( 'admin_menu', 'cm_register_customers' );

/**
 * Display a custom menu page
 */
function show_all_customer(){
    ob_start();
    include('templates/customersContainer.php');
    $output = ob_get_clean();

    echo $output;
}


add_action( 'wp_ajax_customers', 'showCustomer' );
add_action( 'wp_ajax_nopriv_customers', 'showCustomer' );
function showCustomer(){
    global $wpdb;

    $customers = $wpdb->get_results('SELECT * FROM customers');

    wp_send_json(array('customers'=>$customers));
    die();
}



add_action( 'wp_ajax_customerSave', 'save_customer' );
add_action( 'wp_ajax_nopriv_customerSave', 'save_customer' );
function save_customer(){

    global $wpdb;
    $data = $_POST;
    if($data['customer_id'] !=0 or $data['customer_id'] !=''){
        $cid = $data['customer_id'];

        $done = $wpdb->update('customers',$data, array('customer_id'=>$cid));
        $data['mode']='edit';

        wp_send_json($data);
    }
    else{
        $done = $wpdb->insert('customers',$data);
        if(!$done) $wpdb->print_error();
        $data['customer_id'] = $wpdb->insert_id;
        wp_send_json($data);
    }

    die();
}


add_action( 'wp_ajax_customerModify', 'customerModify' );
add_action( 'wp_ajax_nopriv_customerModify', 'customerModify' );
function customerModify(){
    global $wpdb;
    $customer = $wpdb->get_row('SELECT * FROM customers WHERE customer_id='.$_REQUEST['id']);

    wp_send_json($customer);
    die();
}

add_action( 'wp_ajax_customerDelete', 'customerDelete' );
add_action( 'wp_ajax_nopriv_customerDelete', 'customerDelete' );
function customerDelete(){
    global $wpdb;
    $contractexist = $wpdb->get_results('SELECT * FROM contracts WHERE customer_id='.$_GET['id']);
    if(count($contractexist)>0){
        $customer = $wpdb->get_row('SELECT * FROM customers WHERE customer_id='.$_GET['id']);
        wp_send_json(array('success'=>'NO','customer'=>$customer));
        die();
    }
    $customer = $wpdb->query('DELETE FROM customers WHERE customer_id='.$_GET['id']);
    //$customers = $wpdb->get_results('SELECT * FROM customers');
    wp_send_json(array('success'=>'OK','customer'=>$customer));
    die();
}