<?php
/**
 * Register a custom menu page.
 */
function cm_register_paymentschedules() {
    add_submenu_page('all-contracts',
        __( 'Payment Schedule', 'textdomain' ),
        'Payment Schedule',
        'manage_options',
        'all-paymentschedules',
        'show_all_paymentschedule',
        '',
        6
    );
}
add_action( 'admin_menu', 'cm_register_paymentschedules' );

/**
 * Display a custom menu page
 */
function show_all_paymentschedule(){
    ob_start();
    include('templates/paymentscheduleContainer.php');
    $output = ob_get_clean();

    echo $output;
}


add_action( 'wp_ajax_paymentschedules', 'showpaymentschedule' );
add_action( 'wp_ajax_nopriv_paymentschedules', 'showpaymentschedule' );
function showpaymentschedule(){
    global $wpdb;

    $paymentschedules = $wpdb->get_results('SELECT * FROM payment_schedule INNER JOIN contracts ON contracts.contract_id=payment_schedule.contract_id ORDER BY payment_schedule.pymt_sch_id DESC ');
    $contracts = $wpdb->get_results('SELECT * FROM contracts ORDER BY contract_id DESC');
    wp_send_json(array('paymentschedules'=>$paymentschedules,'contracts'=>$contracts));
    die();
}



add_action( 'wp_ajax_paymentscheduleSave', 'save_paymentschedule' );
add_action( 'wp_ajax_nopriv_paymentscheduleSave', 'save_paymentschedule' );
function save_paymentschedule(){

    global $wpdb;
    $data = $_POST;
    if($data['pymt_sch_id'] !=0 or $data['pymt_sch_id'] !=''){
        $cid = $data['pymt_sch_id'];

        $done = $wpdb->update('payment_schedule',$data, array('pymt_sch_id'=>$cid));
        $rowdata =  $wpdb->get_row('SELECT * FROM  contracts where contract_id='.$data['contract_id']);
        $data['mode']='edit';
        $data['contract_title'] = $rowdata->contract_title;

        wp_send_json($data);
    }
    else{
        $done = $wpdb->insert('payment_schedule',$data);
        if(!$done) $wpdb->print_error();
        $data['pymt_sch_id'] = $wpdb->insert_id;
        $rowdata =  $wpdb->get_row('SELECT * FROM  contracts where contract_id='.$data['contract_id']);
        $data['contract_title'] = $rowdata->contract_title;
        wp_send_json($data);
    }

    die();
}


add_action( 'wp_ajax_paymentscheduleModify', 'paymentscheduleModify' );
add_action( 'wp_ajax_nopriv_paymentscheduleModify', 'paymentscheduleModify' );
function paymentscheduleModify(){
    global $wpdb;
    $paymentschedule = $wpdb->get_row('SELECT * FROM payment_schedule WHERE pymt_sch_id='.$_REQUEST['id']);

    wp_send_json($paymentschedule);
    die();
}

add_action( 'wp_ajax_paymentscheduleDelete', 'paymentscheduleDelete' );
add_action( 'wp_ajax_nopriv_paymentscheduleDelete', 'paymentscheduleDelete' );
function paymentscheduleDelete(){
    global $wpdb;

    $paymentschedule = $wpdb->query('DELETE FROM payment_schedule WHERE pymt_sch_id='.$_GET['id']);
    //$paymentschedules = $wpdb->get_results('SELECT * FROM paymentschedules');
    wp_send_json(array('success'=>'OK','paymentschedule'=>$paymentschedule));
    die();
}