<?php
/**
 * Register a custom menu page.
 */
function cm_register_services() {
    add_submenu_page('all-contracts',
        __( 'Services', 'textdomain' ),
        'Services',
        'manage_options',
        'all-services',
        'show_all_service',
        '',
        6
    );
}
add_action( 'admin_menu', 'cm_register_services' );

/**
 * Display a custom menu page
 */
function show_all_service(){
    ob_start();
    include('templates/servicesContainer.php');
    $output = ob_get_clean();

    echo $output;
}


add_action( 'wp_ajax_services', 'showService' );
add_action( 'wp_ajax_nopriv_services', 'showService' );
function showService(){
    global $wpdb;

    $services = $wpdb->get_results('SELECT * FROM services ORDER BY cat_id DESC');
    $services_categories = $wpdb->get_results('SELECT * FROM service_categories');

    wp_send_json(array('services'=>$services,'service_categories'=>$services_categories));
    die();
}



add_action( 'wp_ajax_serviceSave', 'save_service' );
add_action( 'wp_ajax_nopriv_serviceSave', 'save_service' );
function save_service(){

    global $wpdb;
    $data = $_POST;
    if($data['service_id'] !=0 or $data['service_id'] !=''){
        $cid = $data['service_id'];

        $done = $wpdb->update('services',$data, array('service_id'=>$cid));
        $data['mode']='edit';
        $customer = $wpdb->get_row('SELECT * FROM service_cateories WHERE customer_id='.$data['customer_id']);
        if($customer)
            $data['customer_name'] = $customer->customer_name;

        wp_send_json($data);
    }
    else{
        $done = $wpdb->insert('services',$data);
        if(!$done) $wpdb->print_error();
        $data['service_id'] = $wpdb->insert_id;
        $customer = $wpdb->get_row('SELECT * FROM services WHERE service_id='.$data['customer_id']);
        if($customer)
            $data['customer_name'] = $customer->customer_name;
        wp_send_json($data);
    }

    die();
}


add_action( 'wp_ajax_serviceModify', 'serviceModify' );
add_action( 'wp_ajax_nopriv_serviceModify', 'serviceModify' );
function serviceModify(){
    global $wpdb;
    $service = $wpdb->get_row('SELECT * FROM services WHERE service_id='.$_REQUEST['id']);

    wp_send_json($service);
    die();
}

add_action( 'wp_ajax_serviceDelete', 'serviceDelete' );
add_action( 'wp_ajax_nopriv_serviceDelete', 'serviceDelete' );
function serviceDelete(){
    global $wpdb;
    $result = $wpdb->query('DELETE FROM services WHERE service_id='.$_REQUEST['id']);
    if($result){
        wp_send_json(array('susccess'=>'OK'));
    }
    else{
        wp_send_json(array('susccess'=>'NO'));
    }
    die();
}