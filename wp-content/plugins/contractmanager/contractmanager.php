<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              aptech.com
 * @since             1.0.0
 * @package           Contractmanager
 *
 * @wordpress-plugin
 * Plugin Name:       Contracts
 * Plugin URI:        aptech.com
 * Description:       This is a short description of what the plugin does. It's displayed in the WordPress admin area.
 * Version:           1.0.0
 * Author:            Manzurul Haque
 * Author URI:        ajtechbd.com
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       contractmanager
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-contractmanager-activator.php
 */
function activate_contractmanager() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-contractmanager-activator.php';
	Contractmanager_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-contractmanager-deactivator.php
 */
function deactivate_contractmanager() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-contractmanager-deactivator.php';
	Contractmanager_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_contractmanager' );
register_deactivation_hook( __FILE__, 'deactivate_contractmanager' );

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-contractmanager.php';
require plugin_dir_path( __FILE__ ) . 'includes/contracts.php';
require plugin_dir_path( __FILE__ ) . 'includes/contractservices.php';
require plugin_dir_path( __FILE__ ) . 'includes/services.php';
require plugin_dir_path( __FILE__ ) . 'includes/category.php';
require plugin_dir_path( __FILE__ ) . 'includes/customers.php';
require plugin_dir_path( __FILE__ ) . 'includes/staffs.php';
require plugin_dir_path( __FILE__ ) . 'includes/payments.php';
require plugin_dir_path( __FILE__ ) . 'includes/paymentschedule.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_contractmanager() {

	$plugin = new Contractmanager();
	$plugin->run();

}
run_contractmanager();
